package com.dyj.common.enums;

public enum DyAppletUrlPathEnum {

    T_T_DOMAIN("ttDomain","https://developer.toutiao.com"),

    //BusinessToken 生成
    BUSINESS_ACCESS_TOKEN("bizAccessToken", "/oauth/business_token/"),

    //BusinessToken 刷新
    BUSINESS_ACCESS_TOKEN_REFRESH("bizTokenRefresh", "/oauth/refresh_biz_token/"),

    //经营能力当前状态查询
    BUSINESS_SCOPES("businessScopes", "/oauth/business_scopes/"),

    //getAccessToken
    APPS_V2_TOKEN("appsV2Token", "/api/apps/v2/token/"),

    //登录code2Session
    CODE_TO_SESSION("code2Session", "/api/apps/v2/jscode2session"),

    //生成SchemaV2
    GENERATE_SCHEMA("generateSchema", "/api/apps/v1/url/generate_schema/"),
    //查询SchemaV2
    QUERY_SCHEMA("querySchema", "/api/apps/v1/url/query_schema/"),
    //查询Schema配额V2
    QUERY_SCHEMA_QUOTA("querySchemaQuota", "/api/apps/v1/url/query_schema_quota/"),
    //生成 Link V2
    GENERATE_URL_LINK("generateUrlLink", "/api/apps/v1/url_link/generate/"),
    //查询 Link 配额V2
    QUERY_QUOTA_URL_LINK("queryQuotaUrlLink", "/api/apps/v1/url_link/query_quota/"),
    //查询 Link V2
    QUERY_INFO_URL_LINK("queryInfoUrlLink", "/api/apps/v1/url_link/query_info/"),
    //生成QRCodeV2
    CREATE_QR_CODE("createQrCode", "/api/apps/v1/qrcode/create/"),

    //发送主动私信
    AUTHORIZE_SEND_MSG("authSendMsg","/im/authorize/send/msg/"),

    //查询主动私信用户授权状态
    IM_AUTHORIZE_STATUS("imAuthStatus","/im/authorize/status/"),

    //创建线索组件
    CREATE_CLUE_COMPONENT("createClueComponent","/api/apps/v1/capacity/create_clue_component_info"),

    //查询已创建的线索组件
    QUERY_CLUE_COMPONENT("queryClueComponent","/api/apps/v1/capacity/query_clue_component_info"),
    //更新线索组件
    UPDATE_CLUE_COMPONENT("updateClueComponent","/api/apps/v1/capacity/update_clue_component_info"),
    //删除线索组件
    DELETE_CLUE_COMPONENT("deleteClueComponent","/api/apps/v1/capacity/delete_clue_component_info"),

    //查询特定视频的视频数据
    QUERY_VIDEO_LIST("queryAptVideoList", "/api/apps/v1/video/query/"),

    //查询特定视频的视频数据(经营授权)
    QUERY_BIZ_VIDEO_LIST("queryBizVideoList", "/api/apps/v1/video_bc/query/"),
    //获取视频基础数据
    GET_ITEM_BASE_INFO("getItemBaseInfo", "/api/apps/v1/item/get_base/"),
    //获取视频基础数据（经营授权）
    GET_ITEM_BIZ_BASE_INFO("getItemBizBaseInfo", "/api/apps/v1/item_bc/get_base/"),
    //获取视频点赞数据
    GET_ITEM_LIKE_INFO("getItemLikeInfo", "/api/apps/v1/item/get_like/"),
    //获取视频点赞数据（经营授权）
    GET_ITEM_BIZ_LIKE_INFO("getItemBizLikeInfo", "/api/apps/v1/item_bc/get_like/"),

    //获取视频评论数据
    GET_ITEM_COMMENT_INFO("getItemComment", "/api/apps/v1/item/get_comment/"),
    //获取视频评论数据（经营授权）
    GET_ITEM_BIZ_COMMENT_INFO("getItemBizComment", "/api/apps/v1/item_bc/get_comment/"),

    //获取视频播放数据
    GET_ITEM_PLAY_INFO("getItemPlay", "/api/apps/v1/item/get_play/"),

    //获取视频播放数据（经营授权）
    GET_ITEM_BIZ_PLAY_INFO("getItemBizPlay", "/api/apps/v1/item_bc/get_play/"),

    //获取视频分享数据
    GET_ITEM_SHARE_INFO("getItemShare", "/api/apps/v1/item/get_share/"),

    //获取视频分享数据（经营授权）
    GET_ITEM_BIZ_SHARE_INFO("getItemBizShare", "/api/apps/v1/item_bc/get_share/"),
    //置顶评论
    TOP_COMMENT("topComment","/api/apps/v1/item/top_comment/"),
    //评论列表
    COMMENT_LIST("commentList","/api/apps/v1/item/list_comment/"),
    //评论回复列表
    COMMENT_REPLY_LIST("commentReplyList","/api/apps/v1/item/list_comment_reply/"),
    //回复视频评论
    REPLY_COMMENT("replyComment","/api/apps/v1/item/reply_comment/"),
    //videoid转换itemid
    VIDEO_ID_TO_OPEN_ITEM_ID("videoId2itemId","/api/apps/v1/convert_video_id/video_id_to_open_item_id/"),
    //itemid转换encryptid
    ITEM_ID_TO_ENCRYPT_ID("itemId2encryptId","/api/apps/v1/convert_video_id/open_item_id_to_encrypt_id/"),

    //删除抖音搜索直达子服务
    DELETE_SEARCH_SUB_SERVICE("deleteSearchSubService","/aweme/apps/v1/search/delete_sub_service/"),
    //校验是否有「搜索直达服务」的创建权限
    CHECK_SEARCH_SUB_SERVICE("checkSearchSubService","/aweme/apps/v1/search/check_sub_service/"),
    //创建抖音搜索直达子服务
    CREATE_SEARCH_SUB_SERVICE("createSearchSubService","/api/apps/v1/capacity/create_ma_sub_service/"),
    //查询已创建的抖音搜索直达子服务列表
    QUERY_SEARCH_SUB_SERVICE("querySearchSubService","/api/apps/v1/capacity/query_ma_sub_service/"),
    //创建直播任务
    CREATE_LIVE_TASK("createLiveTask","/api/apps/v2/task/create_live/"),
    //创建视频任务
    CREATE_VIDEO_TASK("createVideoTask","/api/apps/v2/task/create_video/"),
    //核销直播任务
    WRITE_OFF_LIVE_TASK("writeOffOutLiveTask","/api/apps/v2/task/writeoff_live/"),
    //核销视频任务
    WRITE_OFF_VIDEO_TASK("writeOffVideoTask","/api/apps/v2/task/writeoff_video/"),

    //注册小程序积分阈值
    LIMIT_OP_POINT("limitOpPoint","/api/ecom/v1/config/limit_op_point/"),
    //注册小程序预览图、定制类小程序开发者注册信息
    REGISTER_MA_APP("registerMaApp","/api/ecom/v1/config/register_ma_app/"),
    //查询订单的定制完成状态
    QUERY_ORDER_CUSTOMIZATION_STATUS("queryOrderCustomizationStatus", "/api/ecom/v1/customization/query_status/"),
    //退会
    SHOP_MEMBER_LEAVE("shopMemberLeave","/api/ecom/v1/shop_member/leave/"),

    //商铺同步
    SUPPLIER_SYNC("supplierSync", "/poi/supplier/sync/"),
    //查询店铺
    QUERY_SUPPLIER("querySupplier", "/poi/supplier/query/"),
    //获取抖音POI ID
    QUERY_POI_ID("queryPoiId", "/poi/query/"),
    //店铺匹配任务结果查询
    QUERY_SUPPLIER_TASK_RESULT("querySupplierTaskResult", "/poi/v2/supplier/query/task/"),
    //店铺匹配状态查询
    QUERY_SUPPLIER_MATCH_STATUS("querySupplierMatchStatus", "/poi/v2/supplier/query/supplier/"),
    //提交门店匹配任务
    SUBMIT_SUPPLIER_MATCH_TASK("submitSupplierMatchTask", "/poi/v2/supplier/match/"),
    //查询全部店铺信息接口(天级别请求5次)
    QUERY_ALL_SUPPLIER("queryAllSupplier", "/poi/supplier/query_all/"),
    //查询店铺全部信息任务返回内容
    QUERY_SUPPLIER_CALLBACK("querySupplierCallback", "/poi/supplier/query_callback/"),

    //（老版本）SKU同步
    SKU_SYNC("skuSync", "/poi/sku/sync/"),
    //（老版本）sku拉取(该接口由接入方实现)
    SKU_HOTEL_PULL("skuHotelPull", "/poi/ext/hotel/sku/"),
    //（老版本）多门店SPU同步
    SPU_SYNC("spuSync", "/poi/v2/spu/sync/"),
    //（老版本）多门店SPU状态同步
    SPU_STATUS_SYNC("spuStatusSync", "/poi/v2/spu/status_sync/"),
    //（老版本）多门店SPU库存同步
    SPU_STOCK_SYNC("spuStockSync", "/poi/v2/spu/stock_update/"),
    //（老版本）多门店SPU信息查询
    SPU_QUERY("spuQuery", "/poi/v2/spu/get/"),
    //创建/修改团购商品
    SAVE_GOODS_PRODUCT("saveGoodsProduct", "/life/goods/product/save/"),
    //免审修改商品
    FREE_AUDIT_GOODS_PRODUCT("freeAuditGoodsProduct", "/life/goods/product/free_audit/"),
    //上下架商品
    OPERATE_GOODS_PRODUCT("operateGoodsProduct", "/life/goods/product/operate/"),
    //同步库存
    SYNC_GOODS_STOCK("syncGoodsStock", "/life/goods/stock/sync/"),
    //查询商品模板
    GET_GOODS_TEMPLATE("getGoodsTemplate", "/life/goods/template/get/"),
    //查询商品草稿数据
    GET_GOODS_PRODUCT_DRAFT("getGoodsProductDraft", "/life/goods/product/draft/get/"),
    //查询商品线上数据
    GET_GOODS_PRODUCT_ONLINE("getGoodsProductOnline", "/life/goods/product/online/get/"),
    //查询商品线上数据列表
    QUERY_GOODS_PRODUCT_ONLINE_LIST("queryGoodsProductOnlineList", "/life/goods/product/online/list/"),
    //查询商品草稿数据列表
    QUERY_GOODS_PRODUCT_DRAFT_LIST("queryGoodsProductDraftList", "/life/goods/product/draft/list/"),
    //创建/更新多SKU商品的SKU列表
    BATCH_SAVE_GOODS_SKU("batchSaveGoodsSku", "/life/goods/sku/batch_save/"),
    //查询商品品类
    GET_GOODS_CATEGORY("getGoodsCategory", "/life/goods/category/get/"),
    //订单同步
    SYNC_ORDER("syncOrder", "/poi/order/sync/"),
    //获取POI基础数据
    QUERY_POI_BASE_DATA("queryPoiBaseData", "/data/external/poi/base/"),
    //POI用户数据
    QUERY_POI_USER_DATA("queryPoiUserData", "/data/external/poi/user/"),
    //POI服务基础数据
    QUERY_POI_SERVICE_BASE_DATA("queryPoiServiceBaseData", "/data/external/poi/service_base/"),
    //POI服务成交用户数据
    QUERY_POI_SERVICE_TRANSACTION_USER_DATA("queryPoiServiceTransactionUserData", "/data/external/poi/service_user/"),
    //POI热度榜
    QUERY_POI_BILLBOARD("queryPoiBillboard", "/data/external/poi/billboard/"),
    //POI认领列表
    QUERY_POI_CLAIM_LIST("queryPoiClaimList", "/data/external/poi/claim/list/"),
    //通过高德POI ID获取抖音POI ID
    QUERY_AMAP_POI_ID("queryAmapPoiId", "/poi/base/query/amap/"),
    //优惠券同步
    SYNC_V2_COUPON("syncV2Coupon", "/poi/v2/coupon/sync/"),
    //优惠券更新
    SYNC_V2_COUPON_AVAILABLE("syncV2CouponAvailable", "/poi/v2/coupon/sync/coupon_available/"),
    //通用佣金计划查询带货数据
    QUERY_COMMON_PLAN_SELL_DETAIL("queryCommonPlanSellDetail", "/api/match/v2/poi/common_plan_sell_detail/"),
    //通用佣金计划查询带货达人列表
    QUERY_COMMON_PLAN_TALENT_LIST("queryCommonPlanTalentList", "/api/match/v2/poi/query_common_plan_talent_list/"),
    //通用佣金计划查询达人带货数据
    QUERY_COMMON_PLAN_TALENT_SELL_DETAIL("queryCommonPlanTalentSellDetail", "/api/match/v2/poi/common_plan_talent_detail/"),
    //通用佣金计划查询达人带货详情
    QUERY_COMMON_PLAN_TALENT_MEDIA_LIST("queryCommonPlanTalentMediaList", "/api/match/v2/poi/common_plan_talent_media_list/"),
    //查询通用佣金计划
    QUERY_COMMON_PLAN_LIST("queryCommonPlanList", "/api/match/v2/poi/list_plan_by_spuid/"),
    //发布/修改通用佣金计划
    SAVE_COMMON_PLAN("saveCommonPlan", "/api/match/v2/poi/save_common_plan/"),
    //修改通用佣金计划状态
    UPDATE_COMMON_PLAN_STATUS("updateCommonPlanStatus", "/api/match/v2/poi/update_common_plan_status/"),
    //发布/修改直播间定向佣金计划
    SAVE_LIVE_PLAN("saveLivePlan", "/api/match/v2/poi/save_live_oriented_plan/"),
    //发布/修改短视频定向佣金计划
    SAVE_SHORT_VIDEO_PLAN("saveShortVideoPlan", "/api/match/v2/poi/save_video_oriented_plan/"),
    //修改定向佣金计划状态
    UPDATE_PLAN_STATUS("updatePlanStatus", "/api/match/v2/poi/update_oriented_plan_status/"),
    //取消定向佣金计划指定的达人
    DELETE_PLAN_TALENT("deletePlanTalent", "/api/match/v2/poi/delete_oriented_plan_talent/"),
    //查询达人的定向佣金计划带货数据
    QUERY_ORIENTED_PLAN_TALENT_SELL_DETAIL("queryOrientedPlanTalentSellDetail", "/api/match/v2/poi/oriented_plan_talent_detail/"),
    //通过商品 ID 查询定向佣金计划
    QUERY_ORIENTED_PLAN_LIST("queryOrientedPlanList", "/api/match/v2/poi/oriented_plan_list/"),
    //查询定向佣金计划带货汇总数据
    QUERY_ORIENTED_PLAN_SELL_DETAIL("queryOrientedPlanSellDetail", "/api/match/v2/poi/oriented_plan_detail/"),


    /**
     * 查询用户可用券信息
     */
    QUERY_COUPON_RECEIVE_INFO("queryCouponReceiveInfo", "/api/promotion/v1/coupon/get_coupon_receive_info/"),

    /**
     * 用户撤销核销券
     */
    BATCH_ROLLBACK_CONSUME_COUPON("batchRollbackConsumeCoupon", "/api/promotion/v1/coupon/batch_rollback_consume_coupon/"),

    /**
     * 复访营销活动实时圈选用户
     */
    BIND_USER_TO_SIDEBAR_ACTIVITY("bindUserToSidebarActivity", "/api/promotion/v2/activity/bind_user_to_sidebar_activity/"),

    /**
     * 用户核销券
     */
    BATCH_CONSUME_COUPON("batchConsumeCoupon", "/api/promotion/v1/coupon/batch_consume_coupon/"),

    /**
     * 查询主播发券配置信息
     */
    QUERY_TALENT_COUPON_LIMIT("queryTalentCouponLimit", "/api/promotion/v1/coupon/get_talent_coupon/"),

    /**
     * 修改主播发券权限状态
     */
    UPDATE_TALENT_COUPON_STATUS("updateTalentCouponStatus", "/api/promotion/v1/coupon/update_talent_coupon_status/"),

    /**
     * 更新主播发券库存上限
     */
    UPDATE_TALENT_COUPON_STOCK("updateTalentCouponStock", "/api/promotion/v1/coupon/update_talent_coupon_stock/"),

    /**
     * 主播发券权限配置
     */
    SET_TALENT_COUPON_API("setTalentCouponApi", "/api/promotion/v1/coupon/set_talent_coupon/"),

    /**
     * 创建营销活动
     */
    CREATE_PROMOTION_ACTIVITY_V2("createPromotionActivityV2", "/api/promotion/v2/activity/create_promotion_activity/"),

    /**
     * 修改营销活动
     */
    MODIFY_PROMOTION_ACTIVITY_V2("modifyPromotionActivityV2", "/api/promotion/v2/activity/modify_promotion_activity/"),

    /**
     * 查询营销活动
     */
    QUERY_PROMOTION_ACTIVITY_V2("queryPromotionActivityV2", "/api/promotion/v2/activity/query_promotion_activity/"),

    /**
     * 修改营销活动状态
     */
    UPDATE_PROMOTION_ACTIVITY_STATUS_V2("updatePromotionActivityStatusV2", "/api/promotion/v2/activity/update_promotion_activity_status/"),

    /**
     * 创建券模板
     */
    CREATE_COUPON_META_V2("createCouponMetaV2", "/api/promotion/v2/coupon/create_coupon_meta/"),

    /**
     * 修改券模板
     */
    MODIFY_COUPON_META_V2("modifyCouponMetaV2", "/api/promotion/v2/coupon/modify_coupon_meta/"),

    /**
     * 查询券模板
     */
    QUERY_COUPON_META_V2("queryCouponMetaV2", "/api/promotion/v2/coupon/query_coupon_meta/"),


    /**
     * 查询授权用户发放的活动信息
     */
    QUERY_ACTIVITY_META_DETAIL_LIST("queryActivityMetaDetailList", "/api/promotion/v1/coupon/query_activity_meta_data/"),

    /**
     * 删除券模板
     */
    CANCEL_COUPON_META_API("cancelCouponMetaApi", "/api/promotion/v1/coupon/delete_coupon_meta/"),

    /**
     * 修改券模板库存
     */
    UPDATE_COUPON_META_STOCK_API("updateCouponMetaStockApi", "/api/promotion/v1/coupon/update_coupon_meta_stock/"),

    /**
     * 修改券模板状态
     */
    UPDATE_COUPON_META_STATUS("updateCouponMetaStatus", "/api/promotion/v1/coupon/update_coupon_meta_status/"),

    /**
     * 查询券模板发放统计数据
     */
    QUERY_COUPON_META_STATISTICS("queryCouponMetaStatistics", "/api/promotion/v1/coupon/get_coupon_meta_statistics/"),

    /**
     * 查询对账单
     */
    GET_BILL_DOWNLOAD_URL("getBillDownloadUrl", "/api/promotion/v1/coupon/get_bill_download_url/"),


    /**
     * 创建开发者接口发券活动
     */
    CREATE_DEVELOPER_ACTIVITY("createDeveloperActivity", "/api/promotion/v1/coupon/create_developer_activity/"),

    /**
     * 开发者接口发券
     */
    SEND_COUPON_TO_DESIGNATED_USER("sendCouponToDesignatedUser", "/api/promotion/v1/coupon/send_coupon_to_designated_user/"),

    /**
     * 删除开发者接口发券活动
     */
    DELETE_DEVELOPER_ACTIVITY("deleteDeveloperActivity", "/api/promotion/v1/coupon/delete_developer_activity/"),

    /**
     * 接口进件图片上传
     */
    MERCHANT_IMAGE_UPLOAD("merchantImageUpload","/api/apps/ecpay/v3/saas/image_upload/"),
    /**
     * 发起进件
     */
    CREATE_SUB_MERCHANT_V3("createSubMerchantV3", "/api/apps/ecpay/v3/saas/create_merchant/"),

    /**
     * 进件查询
     */
    QUERY_MERCHANT_STATUS_V3("queryMerchantStatusV3", "/api/apps/ecpay/v3/saas/query_merchant_status/"),

    /**
     * 开发者获取小程序收款商户/合作方进件页面
     */
    OPEN_APP_ADD_SUB_MERCHANT("openAppAddSubMerchant", "/api/apps/ecpay/v3/saas/app_add_sub_merchant/"),

    /**
     * 服务商获取小程序收款商户进件页面
     */
    OPEN_ADD_APP_MERCHANT("openAddAppMerchant", "/api/apps/ecpay/v3/saas/get_app_merchant/"),

    /**
     * 服务商获取服务商进件页面
     */
    OPEN_SAAS_ADD_MERCHANT("openSaasAddMerchant", "/api/apps/ecpay/v3/saas/add_merchant/"),

    /**
     * 服务商获取合作方进件页面
     */
    OPEN_SAAS_ADD_SUB_MERCHANT("openSaasAddSubMerchant", "/api/apps/ecpay/v3/saas/add_sub_merchant/"),

    /**
     * 查询标签组信息
     */
    TAG_QUERY("tagQuery", "/api/trade_basic/v1/developer/tag_query/"),

    /**
     * 查询CPS信息
     */
    QUERY_CPS_OPEN_API("queryCpsOpenApi", "/api/trade_basic/v1/developer/query_cps/"),

    /**
     * 查询订单信息
     */
    QUERY_ORDER_OPEN_API("queryOrderOpenApi", "/api/trade_basic/v1/developer/order_query/"),

    /**
     * 发起退款
     */
    CREATE_REFUND_OPEN_API("createRefundOpenApi", "/api/trade_basic/v1/developer/refund_create/"),

    /**
     * 查询退款
     */
    QUERY_REFUND_OPEN_API("queryRefundOpenApi", "/api/trade_basic/v1/developer/refund_query/"),

    /**
     * 同步退款审核结果
     */
    MERCHANT_AUDIT_CALLBACK("merchantAuditCallback", "/api/trade_basic/v1/developer/refund_audit_callback/"),

    /**
     * 推送履约状态
     */
    FULFILL_PUSH_STATUS("fulfillPushStatus", "/api/trade_basic/v1/developer/fulfill_push_status"),

    /**
     * 发起分账
     */
    CREATE_SETTLE("createSettle", "/api/trade_basic/v1/developer/settle_create/"),

    /**
     * 查询分账
     */
    QUERY_SETTLE("querySettle", "/api/trade_basic/v1/developer/settle_query/"),

    /**
     * 商户余额查询
     */
    OPEN_QUERY_CHANNEL_BALANCE_ACCOUNT("openQueryChannelBalanceAccount", "/api/apps/ecpay/saas/query_merchant_balance/"),

    /**
     * 商户提现
     */
    OPEN_MERCHANT_WITHDRAW("openMerchantWithdraw", "/api/apps/ecpay/saas/merchant_withdraw/"),

    /**
     * 商户提现结果查询
     */
    OPEN_QUERY_WITHDRAW_ORDER("openQueryWithdrawOrder", "/api/apps/ecpay/saas/query_withdraw_order/"),

    /**
     * 开发者获取小程序收款商户/合作方提现页面
     */
    SAAS_APP_ADD_SUB_MERCHANT("saasAppAddSubMerchant","/api/apps/ecpay/v3/saas/app_add_sub_merchant/"),

    /**
     * 服务商获取小程序收款商户提现页面
     */
    SAAS_GET_APP_MERCHANT("saasGetAppMerchant","/api/apps/ecpay/v3/saas/get_app_merchant/"),

    /**
     * 服务商获取服务商提现页面
     */
    SAAS_ADD_MERCHANT("saasAddMerchant","/api/apps/ecpay/v3/saas/add_merchant/"),

    /**
     * 服务商获取合作方提现页面
     */
    SAAS_ADD_SUB_MERCHANT("saasAddSubMerchant","/api/apps/ecpay/v3/saas/add_sub_merchant/"),

    /**
     * 获取资金账单
     */
    GET_FUND_BILL("getFundBill", "/api/apps/v3/fund/bills/"),

    /**
     * 获取交易账单
     */
    GET_BILL("getBill", "/api/apps/v3/bills/"),
    ;




    private String key;
    private String value;

    DyAppletUrlPathEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public static String getValueByKey(String key) {
        for (DyAppletUrlPathEnum e : DyAppletUrlPathEnum.values()) {
            if (e.getKey().equals(key)) {
                return e.getValue();
            }
        }
        return null;
    }
}
