package com.dyj.applet.handler;

import com.dyj.applet.domain.query.OpenTrafficPermissionQuery;
import com.dyj.applet.domain.vo.AdIncomeVo;
import com.dyj.applet.domain.vo.AdPlacementVo;
import com.dyj.applet.domain.vo.TrafficPermissionVo;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DySimpleResult;

/**
 * @author danmo
 * @date 2024-06-17 15:37
 **/
public class AptTrafficPermissionHandler extends AbstractAppletHandler {
    public AptTrafficPermissionHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    /**
     * 查询流量主开通状态
     *
     * @return DySimpleResult<TrafficPermissionVo>
     */
    public DySimpleResult<TrafficPermissionVo> queryTrafficPermission() {
        return getTrafficPermissionClient().queryTrafficPermission(baseQuery());
    }


    /**
     * 开通流量主
     *
     * @return DySimpleResult<String>
     */
    public DySimpleResult<String> openTrafficPermission(OpenTrafficPermissionQuery query) {
        return getTrafficPermissionClient().openTrafficPermission(query);
    }


    /**
     * 查询广告位列表
     *
     * @return DySimpleResult<AdPlacementVo>
     */
    public DySimpleResult<AdPlacementVo> queryAdPlacementList() {
        return getTrafficPermissionClient().queryAdPlacementList(baseQuery());
    }


    /**
     * 新增广告位
     *
     * @param adPlacementName 广告位名称
     * @param adPlacementType 广告位类型 1：Banner广告 2：激励视频广告 3：信息流广告 4：插屏广告 5：视频前贴片广告 6：视频后贴片广告
     * @return DySimpleResult<String>
     */
    public DySimpleResult<String> addAdPlacement(String adPlacementName, Integer adPlacementType) {
        return getTrafficPermissionClient().addAdPlacement(baseQuery(), adPlacementName, adPlacementType);
    }

    /**
     * 更新广告位
     *
     * @param adPlacementId 广告位ID
     * @param status        广告位状态  0：关闭  1：开启
     * @return DySimpleResult<String>
     */
    public DySimpleResult<String> updateAdPlacement(String adPlacementId, Integer status) {
        return getTrafficPermissionClient().updateAdPlacement(baseQuery(), adPlacementId, status);
    }

    /**
     * 查询广告收入
     *
     * @param startDate 开始日期，格式“2006-01-02”
     * @param endDate   结束日期，格式“2006-01-02”，查询时间范围不能超过90天
     * @param hostName  宿主APP  douyin-抖音 douyin_lite-抖音 litetoutiao-今日头条 tt_lite-今日头条 litehuoshan-抖音火山版
     * @return DySimpleResult<AdIncomeVo>
     */
    public DySimpleResult<AdIncomeVo> queryAdIncome(String startDate, String endDate, String hostName) {
        return getTrafficPermissionClient().queryAdIncome(baseQuery(), startDate, endDate, hostName);
    }

}
