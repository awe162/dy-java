package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AptUserProfile;
import com.dyj.common.domain.vo.BaseVo;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-24 14:22
 **/
public class AptUserProfileVo extends BaseVo {

    private List<AptUserProfile> result_list;

    public List<AptUserProfile> getResult_list() {
        return result_list;
    }

    public void setResult_list(List<AptUserProfile> result_list) {
        this.result_list = result_list;
    }
}
