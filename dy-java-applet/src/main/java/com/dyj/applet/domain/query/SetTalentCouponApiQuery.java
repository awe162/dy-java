package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.UserInfoQuery;

/**
 * 主播发券权限配置请求值
 */
public class SetTalentCouponApiQuery extends UserInfoQuery {

    /**
     * <p>账号类型：取值1时：talent_account 字段必填 取值2是：open_id 字段必填
     */
    private Integer account_type;
    /**
     * <p>小程序appid</p>
     */
    private String app_id;
    /**
     * <p>分配给主播奖励券的库存数，仅券模板玩法含奖励券时必填</p> 选填
     */
    private Long award_stock_limit;
    /**
     * <p>抖音开平券模板id（券模板创建接口返回）</p>
     */
    private String coupon_meta_id;

    /**
     * <p>分配给主播的券库存的发放上限，不得超过券模板最大库存。平台不会校验已授权所有主播的库存之和是否大于券模板总库存，但主播真实可发券数量受总库存影响。例如券模板库存1000，开发者授权给了A、B、C三位主播每人1000，当A、B主播实际每人发放500后，A、B、C三人均不能再发券。</p>
     */
    private Long stock_limit;
    /**
     * <p>主播抖音号</p> 选填
     */
    private String talent_account;

    public Integer getAccount_type() {
        return account_type;
    }

    public SetTalentCouponApiQuery setAccount_type(Integer account_type) {
        this.account_type = account_type;
        return this;
    }

    public String getApp_id() {
        return app_id;
    }

    public SetTalentCouponApiQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public Long getAward_stock_limit() {
        return award_stock_limit;
    }

    public SetTalentCouponApiQuery setAward_stock_limit(Long award_stock_limit) {
        this.award_stock_limit = award_stock_limit;
        return this;
    }

    public String getCoupon_meta_id() {
        return coupon_meta_id;
    }

    public SetTalentCouponApiQuery setCoupon_meta_id(String coupon_meta_id) {
        this.coupon_meta_id = coupon_meta_id;
        return this;
    }

    public Long getStock_limit() {
        return stock_limit;
    }

    public SetTalentCouponApiQuery setStock_limit(Long stock_limit) {
        this.stock_limit = stock_limit;
        return this;
    }

    public String getTalent_account() {
        return talent_account;
    }

    public SetTalentCouponApiQuery setTalent_account(String talent_account) {
        this.talent_account = talent_account;
        return this;
    }

    public static SetTalentCouponApiQueryBuilder builder(){
        return new SetTalentCouponApiQueryBuilder();
    }

    public static final class SetTalentCouponApiQueryBuilder {
        private Integer account_type;
        private String app_id;
        private Long award_stock_limit;
        private String coupon_meta_id;
        private Long stock_limit;
        private String talent_account;
        private String open_id;
        private Integer tenantId;
        private String clientKey;

        private SetTalentCouponApiQueryBuilder() {
        }

        public static SetTalentCouponApiQueryBuilder aSetTalentCouponApiQuery() {
            return new SetTalentCouponApiQueryBuilder();
        }

        public SetTalentCouponApiQueryBuilder accountType(Integer accountType) {
            this.account_type = accountType;
            return this;
        }

        public SetTalentCouponApiQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public SetTalentCouponApiQueryBuilder award_stock_limit(Long award_stock_limit) {
            this.award_stock_limit = award_stock_limit;
            return this;
        }

        public SetTalentCouponApiQueryBuilder couponMetaId(String couponMetaId) {
            this.coupon_meta_id = couponMetaId;
            return this;
        }

        public SetTalentCouponApiQueryBuilder stockLimit(Long stockLimit) {
            this.stock_limit = stockLimit;
            return this;
        }

        public SetTalentCouponApiQueryBuilder talentAccount(String talentAccount) {
            this.talent_account = talentAccount;
            return this;
        }

        public SetTalentCouponApiQueryBuilder openId(String openId) {
            this.open_id = openId;
            return this;
        }

        public SetTalentCouponApiQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public SetTalentCouponApiQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public SetTalentCouponApiQuery build() {
            SetTalentCouponApiQuery setTalentCouponApiQuery = new SetTalentCouponApiQuery();
            setTalentCouponApiQuery.setAccount_type(account_type);
            setTalentCouponApiQuery.setApp_id(app_id);
            setTalentCouponApiQuery.setAward_stock_limit(award_stock_limit);
            setTalentCouponApiQuery.setCoupon_meta_id(coupon_meta_id);
            setTalentCouponApiQuery.setStock_limit(stock_limit);
            setTalentCouponApiQuery.setTalent_account(talent_account);
            setTalentCouponApiQuery.setOpen_id(open_id);
            setTalentCouponApiQuery.setTenantId(tenantId);
            setTalentCouponApiQuery.setClientKey(clientKey);
            return setTalentCouponApiQuery;
        }
    }
}
