package com.dyj.applet.domain;

/**
 * 查询分账返回值
 */
public class QuerySettleResult {

    /**
     * <p>佣金，单位分</p>
     */
    private Long commission;
    /**
     * <p>开发者自定义透传字段，长度 <= 2048 字节，不支持二进制数据</p>
     */
    private String cp_extra;
    /**
     * <p>抖音开平侧item单 id，长度 <= 64 字节，由数字、ASCII 字符组成，<strong>按券分账时该字段不为空</strong></p>
     */
    private String item_order_id;
    /**
     * <p>抖音开平侧交易订单 id，长度 <= 64 字节，由数字、ASCII 字符组成</p>
     */
    private String order_id;
    /**
     * <p>开发者侧分账单 id，分账请求的唯一标识，长度 <= 64 字节</p>
     */
    private String out_order_no;
    /**
     * <p>开发者侧分账单 id，分账请求的唯一标识，长度 <= 64 字节</p>
     */
    private String out_settle_no;
    /**
     * 平台优惠补贴金额 选填
     */
    private Long platform_ticket;
    /**
     * <p>手续费，单位分</p>
     */
    private Long rake;
    /**
     * <p>分账金额，单位分</p>
     */
    private Long settle_amount;
    /**
     * <p>分账时间，单位ms</p> 选填
     */
    private Long settle_at;
    /**
     * 分账详情
     */
    private String settle_detail;
    /**
     * <p>抖音开平侧分账单id，长度 <= 64 字节，由数字、ASCII 字符组成</p>
     */
    private String settle_id;
    /**
     * <p>分账状态：</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">INIT：初始化</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">PROCESSING：处理中</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">SUCCESS：处理成功</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">FAIL：处理失败</li></ul>
     */
    private String settle_status;
    /**
     * <p>抖音开平底层分账单id，长度 <= 64 字节，由数字、ASCII 字符组成</p>
     */
    private String wallet_settle_id;

    public Long getCommission() {
        return commission;
    }

    public QuerySettleResult setCommission(Long commission) {
        this.commission = commission;
        return this;
    }

    public String getCp_extra() {
        return cp_extra;
    }

    public QuerySettleResult setCp_extra(String cp_extra) {
        this.cp_extra = cp_extra;
        return this;
    }

    public String getItem_order_id() {
        return item_order_id;
    }

    public QuerySettleResult setItem_order_id(String item_order_id) {
        this.item_order_id = item_order_id;
        return this;
    }

    public String getOrder_id() {
        return order_id;
    }

    public QuerySettleResult setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getOut_order_no() {
        return out_order_no;
    }

    public QuerySettleResult setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public String getOut_settle_no() {
        return out_settle_no;
    }

    public QuerySettleResult setOut_settle_no(String out_settle_no) {
        this.out_settle_no = out_settle_no;
        return this;
    }

    public Long getPlatform_ticket() {
        return platform_ticket;
    }

    public QuerySettleResult setPlatform_ticket(Long platform_ticket) {
        this.platform_ticket = platform_ticket;
        return this;
    }

    public Long getRake() {
        return rake;
    }

    public QuerySettleResult setRake(Long rake) {
        this.rake = rake;
        return this;
    }

    public Long getSettle_amount() {
        return settle_amount;
    }

    public QuerySettleResult setSettle_amount(Long settle_amount) {
        this.settle_amount = settle_amount;
        return this;
    }

    public Long getSettle_at() {
        return settle_at;
    }

    public QuerySettleResult setSettle_at(Long settle_at) {
        this.settle_at = settle_at;
        return this;
    }

    public String getSettle_detail() {
        return settle_detail;
    }

    public QuerySettleResult setSettle_detail(String settle_detail) {
        this.settle_detail = settle_detail;
        return this;
    }

    public String getSettle_id() {
        return settle_id;
    }

    public QuerySettleResult setSettle_id(String settle_id) {
        this.settle_id = settle_id;
        return this;
    }

    public String getSettle_status() {
        return settle_status;
    }

    public QuerySettleResult setSettle_status(String settle_status) {
        this.settle_status = settle_status;
        return this;
    }

    public String getWallet_settle_id() {
        return wallet_settle_id;
    }

    public QuerySettleResult setWallet_settle_id(String wallet_settle_id) {
        this.wallet_settle_id = wallet_settle_id;
        return this;
    }
}
