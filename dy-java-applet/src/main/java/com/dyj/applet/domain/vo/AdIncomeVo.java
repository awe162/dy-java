package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AdIncome;

import java.util.List;

/**
 * @author danmo
 * @date 2024-06-17 15:35
 **/
public class AdIncomeVo {

    private List<AdIncome> income_list;

    public List<AdIncome> getIncome_list() {
        return income_list;
    }

    public void setIncome_list(List<AdIncome> income_list) {
        this.income_list = income_list;
    }
}
