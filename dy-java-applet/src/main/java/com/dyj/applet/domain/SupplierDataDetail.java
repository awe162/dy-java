package com.dyj.applet.domain;

import com.alibaba.fastjson.JSONObject;

import java.util.List;

/**
 * @author danmo
 * @date 2024-04-28 15:04
 **/
public class SupplierDataDetail {

    /**
     * 在线状态 1 - 在线; 2 - 下线
     */
    private Integer status;

    /**
     * 店铺属性字段，编号规则：垂直行业 1xxx-酒店民宿 2xxx-餐饮 3xxx-景区 通用属性-9yxxx
     */
    private JSONObject attributes;

    /**
     * 联系座机号
     */
    private String contact_tel;
    /**
     * 商家资质信息
     */
    private SupplierCustomerInfo customer_info;
    /**
     * 店铺介绍(<=500字)
     */
    private String description;
    /**
     * 店铺名称
     */
    private String name;
    /**
     * poi详细信息
     */
    private SupplierPoiInfo poi_info;
    /**
     * 店铺地址
     */
    private String address;
    /**
     * 抖音poi id, 三方如果使用高德poi id可以通过/poi/query/接口转换，其它三方poi id走poi匹配功能进行抖音poi id获取
     */
    private String poi_id;
    /**
     * 接入方店铺id
     */
    private String supplier_ext_id;
    /**
     * 店铺类型 1 - 酒店民宿 2 - 餐饮 3 - 景区 4 - 电商 5 - 教育 6 - 丽人 7 - 爱车 8 - 亲子 9 - 宠物 10 - 家装 11 - 娱乐场所 12 - 图文快印
     */
    private Integer type;
    /**
     * 联系手机号
     */
    private String contact_phone;
    /**
     * 服务商资质信息
     */
    private SupplierServiceProvider service_provider;
    /**
     * 店铺图片
     */
    private List<SupplierService> images;
    private String services;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public JSONObject getAttributes() {
        return attributes;
    }

    public void setAttributes(JSONObject attributes) {
        this.attributes = attributes;
    }

    public String getContact_tel() {
        return contact_tel;
    }

    public void setContact_tel(String contact_tel) {
        this.contact_tel = contact_tel;
    }

    public SupplierCustomerInfo getCustomer_info() {
        return customer_info;
    }

    public void setCustomer_info(SupplierCustomerInfo customer_info) {
        this.customer_info = customer_info;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SupplierPoiInfo getPoi_info() {
        return poi_info;
    }

    public void setPoi_info(SupplierPoiInfo poi_info) {
        this.poi_info = poi_info;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPoi_id() {
        return poi_id;
    }

    public void setPoi_id(String poi_id) {
        this.poi_id = poi_id;
    }

    public String getSupplier_ext_id() {
        return supplier_ext_id;
    }

    public void setSupplier_ext_id(String supplier_ext_id) {
        this.supplier_ext_id = supplier_ext_id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getContact_phone() {
        return contact_phone;
    }

    public void setContact_phone(String contact_phone) {
        this.contact_phone = contact_phone;
    }

    public SupplierServiceProvider getService_provider() {
        return service_provider;
    }

    public void setService_provider(SupplierServiceProvider service_provider) {
        this.service_provider = service_provider;
    }

    public List<SupplierService> getImages() {
        return images;
    }

    public void setImages(List<SupplierService> images) {
        this.images = images;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }
}
