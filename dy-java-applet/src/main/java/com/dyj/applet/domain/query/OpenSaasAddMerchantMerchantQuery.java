package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseTransactionMerchantQuery;
import com.dyj.common.enums.TransactionMerchantTokenTypeEnum;

/**
 * 服务商获取服务商进件页面请求值
 */
public class OpenSaasAddMerchantMerchantQuery extends BaseTransactionMerchantQuery {


    /**
     * <p>需要进件的支付解决方案</p><p>1：<a href="https://developer.open-douyin.com/docs/resource/zh-CN/mini-app/open-capacity/guaranteed-payment/guide/merchant" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">担保支付｜普通版</a></p><p>2：<a href="https://developer.open-douyin.com/docs/resource/zh-CN/mini-app/open-capacity/guaranteed-payment/guide/merchant-E" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">担保支付｜企业版</a></p><p>不传默认为1</p><p>注意：在同一种支付解决方案下的商户号才可以互相分账</p> 选填
     */
    private Integer prod_id;
    /**
     * <p>小程序第三方平台应用 id</p>
     */
    private String thirdparty_component_id;
    /**
     * <p>链接类型枚举值：1: 进件页面</p>
     */
    private Integer url_type;

    public Integer getProd_id() {
        return prod_id;
    }

    public OpenSaasAddMerchantMerchantQuery setProd_id(Integer prod_id) {
        this.prod_id = prod_id;
        return this;
    }

    public String getThirdparty_component_id() {
        return thirdparty_component_id;
    }

    public OpenSaasAddMerchantMerchantQuery setThirdparty_component_id(String thirdparty_component_id) {
        this.thirdparty_component_id = thirdparty_component_id;
        return this;
    }

    public Integer getUrl_type() {
        return url_type;
    }

    public OpenSaasAddMerchantMerchantQuery setUrl_type(Integer url_type) {
        this.url_type = url_type;
        return this;
    }

    public static OpenSaasAddMerchantQueryBuilder builder() {
        return new OpenSaasAddMerchantQueryBuilder();
    }

    public static final class OpenSaasAddMerchantQueryBuilder {
        private Integer prod_id;
        private String thirdparty_component_id;
        private Integer url_type;
        private TransactionMerchantTokenTypeEnum transactionMerchantTokenType = TransactionMerchantTokenTypeEnum.CLIENT_TOKEN;
        private Integer tenantId;
        private String clientKey;

        private OpenSaasAddMerchantQueryBuilder() {
        }

        public OpenSaasAddMerchantQueryBuilder prodId(Integer prodId) {
            this.prod_id = prodId;
            return this;
        }

        public OpenSaasAddMerchantQueryBuilder thirdpartyComponentId(String thirdpartyComponentId) {
            this.thirdparty_component_id = thirdpartyComponentId;
            return this;
        }

        public OpenSaasAddMerchantQueryBuilder urlType(Integer urlType) {
            this.url_type = urlType;
            return this;
        }

        public OpenSaasAddMerchantQueryBuilder transactionMerchantTokenType(TransactionMerchantTokenTypeEnum transactionMerchantTokenType) {
            this.transactionMerchantTokenType = transactionMerchantTokenType;
            return this;
        }

        public OpenSaasAddMerchantQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public OpenSaasAddMerchantQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public OpenSaasAddMerchantMerchantQuery build() {
            OpenSaasAddMerchantMerchantQuery openSaasAddMerchantQuery = new OpenSaasAddMerchantMerchantQuery();
            openSaasAddMerchantQuery.setProd_id(prod_id);
            openSaasAddMerchantQuery.setThirdparty_component_id(thirdparty_component_id);
            openSaasAddMerchantQuery.setUrl_type(url_type);
            openSaasAddMerchantQuery.setTransactionMerchantTokenType(transactionMerchantTokenType);
            openSaasAddMerchantQuery.setTenantId(tenantId);
            openSaasAddMerchantQuery.setClientKey(clientKey);
            return openSaasAddMerchantQuery;
        }
    }
}
