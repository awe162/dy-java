package com.dyj.applet.domain.query;

import com.alibaba.fastjson.JSONObject;
import com.dyj.applet.domain.*;
import com.dyj.common.domain.query.BaseQuery;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;

/**
 * @author danmo
 * @date 2024-04-28 11:40
 **/
public class SupplierSyncQuery extends BaseQuery {

    /**
     * 联系手机号
     */
    private String contact_phone;
    /**
     * 联系座机号
     */
    private String contact_tel;

    /**
     * 店铺图片
     */
    private List<String> images;

    /**
     * 商户号；商家担保交易中的收款账户ID
     */
    private String merchant_uid;

    /**
     * 服务商资质信息
     */
    private SupplierServiceProvider service_provider;

    /**
     * 接入方店铺id
     */
    private String supplier_ext_id;

    /**
     * 标签
     * [可停车 离地铁近]
     */
    private List<String> tags;

    /**
     * 纬度
     */
    private String latitude;

    /**
     * 在线状态 1 - 在线; 2 - 下线
     */
    private Integer status;

    /**
     * POI品类编码
     */
    private String type_code;

    /**
     * POI品类描述 eg. 美食;中式餐饮;小龙虾
     */
    private String type_name;

    /**
     * 店铺地址
     */
    private String address;

    /**
     * 人均消费（单位分）
     */
    private Long avg_cost;

    /**
     * 商家资质信息
     */
    private SupplierCustomerInfo customer_info;

    /**
     * 店铺介绍(<=500字)
     */
    private String description;

    /**
     * 店铺名称
     */
    private String name;

    /**
     * 推荐
     */
    private List<String> recommends;

    /**
     * 店铺提供的服务列表
     */
    private List<SupplierService> services;

    /**
     * 店铺属性字段，编号规则：垂直行业 1xxx-酒店民宿 2xxx-餐饮 3xxx-景区 通用属性-9yxxx
     */
    private JSONObject attributes;

    /**
     * 经度
     */
    private String longitude;

    /**
     * 营业时间, 从周一到周日，list长度为7，不营业则为空字符串
     * [10:00-12:00;13:00-22:00 10:00-12:00;13:00-22:00 10:00-12:00;13:00-22:00 10:00-12:00;13:00-22:00 10:00-12:00;13:00-22:00 10:00-12:00;13:00-22:00]
     */
    private List<String> open_time;

    /**
     * 抖音poi id, 三方如果使用高德poi id可以通过/poi/query/接口转换，其它三方poi id走poi匹配功能进行抖音poi id获取
     */
    private String poi_id;
    /**
     * 店铺类型
     * 1 - 酒店民宿
     * 2 - 餐饮
     * 3 - 景区
     * 4 - 电商
     * 5 - 教育
     * 6 - 丽人
     * 7 - 爱车
     * 8 - 亲子
     * 9 - 宠物
     * 10 - 家装
     * 11 - 娱乐场所
     * 12 - 图文快印
     */
    private Integer type;

    public static SupplierSyncQueryBuilder builder() {
        return new SupplierSyncQueryBuilder();
    }

    public static class SupplierSyncQueryBuilder {
        private String contactPhone;
        private String contactTel;
        private List<String> images;
        private String merchantUid;
        private SupplierServiceProvider serviceProvider;
        private String supplierExtId;

        private List<String> tags;
        private String latitude;
        private Integer status;
        private String typeCode;
        private String typeName;
        private String address;
        private Long avgCost;
        private SupplierCustomerInfo customerInfo;
        private String description;
        private String name;
        private List<String> recommends;
        private List<SupplierService> services;

        private JSONObject attributes;

        private String longitude;
        private List<String> openTime;
        private String poiId;
        private Integer type;

        public SupplierSyncQueryBuilder contactPhone(String contactPhone) {
            this.contactPhone = contactPhone;
            return this;
        }

        public SupplierSyncQueryBuilder contactTel(String contactTel) {
            this.contactTel = contactTel;
            return this;
        }

        public SupplierSyncQueryBuilder images(List<String> images) {
            this.images = images;
            return this;
        }

        public SupplierSyncQueryBuilder merchantUid(String merchantUid) {
            this.merchantUid = merchantUid;
            return this;
        }

        public SupplierSyncQueryBuilder serviceProvider(SupplierServiceProvider serviceProvider) {
            this.serviceProvider = serviceProvider;
            return this;
        }

        public SupplierSyncQueryBuilder supplierExtId(String supplierExtId) {
            this.supplierExtId = supplierExtId;
            return this;
        }

        public SupplierSyncQueryBuilder tags(List<String> tags) {
            this.tags = tags;
            return this;
        }

        public SupplierSyncQueryBuilder latitude(String latitude) {
            this.latitude = latitude;
            return this;
        }

        public SupplierSyncQueryBuilder status(Integer status) {
            this.status = status;
            return this;
        }

        public SupplierSyncQueryBuilder typeCode(String typeCode) {
            this.typeCode = typeCode;
            return this;
        }

        public SupplierSyncQueryBuilder typeName(String typeName) {
            this.typeName = typeName;
            return this;
        }

        public SupplierSyncQueryBuilder address(String address) {
            this.address = address;
            return this;
        }

        public SupplierSyncQueryBuilder avgCost(Long avgCost) {
            this.avgCost = avgCost;
            return this;
        }

        public SupplierSyncQueryBuilder customerInfo(SupplierCustomerInfo customerInfo) {
            this.customerInfo = customerInfo;
            return this;
        }

        public SupplierSyncQueryBuilder description(String description) {
            this.description = description;
            return this;
        }

        public SupplierSyncQueryBuilder name(String name) {
            this.name = name;
            return this;
        }

        public SupplierSyncQueryBuilder recommends(List<String> recommends) {
            this.recommends = recommends;
            return this;
        }

        public SupplierSyncQueryBuilder services(List<SupplierService> services) {
            this.services = services;
            return this;
        }

        public SupplierSyncQueryBuilder attributes(List<SupplierHotelService> hotelServices
                , List<SupplierHotelFacilitie> hotelFacilities
                , List<SupplierHotelFeatured> hotelFeatures
                , SupplierHotelPolicy hotelPolicy
                , Integer type
                , SupplierScenicNotice scenicNotice) {
            this.setAttributes(hotelServices, hotelFacilities, hotelFeatures, hotelPolicy, type, scenicNotice);
            return this;
        }

        public SupplierSyncQueryBuilder longitude(String longitude) {
            this.longitude = longitude;
            return this;
        }

        public SupplierSyncQueryBuilder openTime(List<String> openTime) {
            this.openTime = openTime;
            return this;
        }

        public SupplierSyncQueryBuilder poiId(String poiId) {
            this.poiId = poiId;
            return this;
        }

        public SupplierSyncQueryBuilder type(Integer type) {
            this.type = type;
            return this;
        }

        /**
         * 设置店铺属性字段，编号规则：垂直行业 1xxx-酒店民宿 2xxx-餐饮 3xxx-景区 通用属性-9yxxx
         *
         * @param hotelServices   酒店服务
         * @param hotelFacilities 酒店设施
         * @param hotelFeatures   酒店特色项目
         * @param hotelPolicy     酒店政策
         * @param type            下单模板。1 - 国内模板, 2 - 海外模板
         * @param scenicNotice    景区须知
         */
        public void setAttributes(List<SupplierHotelService> hotelServices
                , List<SupplierHotelFacilitie> hotelFacilities
                , List<SupplierHotelFeatured> hotelFeatures
                , SupplierHotelPolicy hotelPolicy
                , Integer type
                , SupplierScenicNotice scenicNotice) {
            if (Objects.isNull(this.attributes)) {
                this.attributes = new JSONObject();
            }
            if (!CollectionUtils.isEmpty(hotelServices)) {
                this.attributes.put("1101", hotelServices);
            }
            if (!CollectionUtils.isEmpty(hotelFacilities)) {
                this.attributes.put("1102", hotelFacilities);
            }
            if (!CollectionUtils.isEmpty(hotelFeatures)) {
                this.attributes.put("1103", hotelFeatures);
            }
            if (Objects.nonNull(hotelPolicy)) {
                this.attributes.put("1104", hotelPolicy);
            }
            if (Objects.nonNull(type)) {
                this.attributes.put("1105", type);
            }
            if (Objects.nonNull(scenicNotice)) {
                this.attributes.put("3101", scenicNotice);
            }

        }

        public SupplierSyncQuery build() {
            SupplierSyncQuery supplierSyncQuery = new SupplierSyncQuery();
            supplierSyncQuery.setContact_phone(contactPhone);
            supplierSyncQuery.setContact_tel(contactTel);
            supplierSyncQuery.setImages(images);
            supplierSyncQuery.setMerchant_uid(merchantUid);
            supplierSyncQuery.setService_provider(serviceProvider);
            supplierSyncQuery.setSupplier_ext_id(supplierExtId);
            supplierSyncQuery.setTags(tags);
            supplierSyncQuery.setLatitude(latitude);
            supplierSyncQuery.setStatus(status);
            supplierSyncQuery.setType_code(typeCode);
            supplierSyncQuery.setType_name(typeName);
            supplierSyncQuery.setAddress(address);
            supplierSyncQuery.setAvg_cost(avgCost);
            supplierSyncQuery.setCustomer_info(customerInfo);
            supplierSyncQuery.setDescription(description);
            supplierSyncQuery.setName(name);
            supplierSyncQuery.setRecommends(recommends);
            supplierSyncQuery.setServices(services);
            supplierSyncQuery.setAttributes(attributes);
            supplierSyncQuery.setOpen_time(openTime);
            supplierSyncQuery.setLongitude(longitude);
            supplierSyncQuery.setPoi_id(poiId);
            supplierSyncQuery.setType(type);
            return supplierSyncQuery;
        }

    }


    public String getContact_phone() {
        return contact_phone;
    }

    public void setContact_phone(String contact_phone) {
        this.contact_phone = contact_phone;
    }

    public String getContact_tel() {
        return contact_tel;
    }

    public void setContact_tel(String contact_tel) {
        this.contact_tel = contact_tel;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getMerchant_uid() {
        return merchant_uid;
    }

    public void setMerchant_uid(String merchant_uid) {
        this.merchant_uid = merchant_uid;
    }

    public SupplierServiceProvider getService_provider() {
        return service_provider;
    }

    public void setService_provider(SupplierServiceProvider service_provider) {
        this.service_provider = service_provider;
    }

    public String getSupplier_ext_id() {
        return supplier_ext_id;
    }

    public void setSupplier_ext_id(String supplier_ext_id) {
        this.supplier_ext_id = supplier_ext_id;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getType_code() {
        return type_code;
    }

    public void setType_code(String type_code) {
        this.type_code = type_code;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getAvg_cost() {
        return avg_cost;
    }

    public void setAvg_cost(Long avg_cost) {
        this.avg_cost = avg_cost;
    }

    public SupplierCustomerInfo getCustomer_info() {
        return customer_info;
    }

    public void setCustomer_info(SupplierCustomerInfo customer_info) {
        this.customer_info = customer_info;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getRecommends() {
        return recommends;
    }

    public void setRecommends(List<String> recommends) {
        this.recommends = recommends;
    }

    public List<SupplierService> getServices() {
        return services;
    }

    public void setServices(List<SupplierService> services) {
        this.services = services;
    }

    public JSONObject getAttributes() {
        return attributes;
    }

    public void setAttributes(JSONObject attributes) {
        this.attributes = attributes;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public List<String> getOpen_time() {
        return open_time;
    }

    public void setOpen_time(List<String> open_time) {
        this.open_time = open_time;
    }

    public String getPoi_id() {
        return poi_id;
    }

    public void setPoi_id(String poi_id) {
        this.poi_id = poi_id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
