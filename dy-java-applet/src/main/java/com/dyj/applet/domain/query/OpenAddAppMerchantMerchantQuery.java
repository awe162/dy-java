package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseTransactionMerchantQuery;
import com.dyj.common.enums.TransactionMerchantTokenTypeEnum;

/**
 * 服务商获取小程序收款商户进件页面请求值
 */
public class OpenAddAppMerchantMerchantQuery extends BaseTransactionMerchantQuery {


    /**
     * <p>小程序的app_id</p>
     */
    private String app_id;
    /**
     * <p>小程序第三方平台应用 id</p>
     */
    private String thirdparty_id;
    /**
     * <p>链接类型枚举值：</p><p>1: 进件页面</p>
     */
    private Integer url_type;

    public String getApp_id() {
        return app_id;
    }

    public OpenAddAppMerchantMerchantQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getThirdparty_id() {
        return thirdparty_id;
    }

    public OpenAddAppMerchantMerchantQuery setThirdparty_id(String thirdparty_id) {
        this.thirdparty_id = thirdparty_id;
        return this;
    }

    public Integer getUrl_type() {
        return url_type;
    }

    public OpenAddAppMerchantMerchantQuery setUrl_type(Integer url_type) {
        this.url_type = url_type;
        return this;
    }

    public static OpenAddAppMerchantQueryBuilder builder() {
        return new OpenAddAppMerchantQueryBuilder();
    }

    public static final class OpenAddAppMerchantQueryBuilder {
        private String app_id;
        private String thirdparty_id;
        private Integer url_type;
        private TransactionMerchantTokenTypeEnum transactionAccessTokenType = TransactionMerchantTokenTypeEnum.CLIENT_TOKEN;
        private Integer tenantId;
        private String clientKey;

        private OpenAddAppMerchantQueryBuilder() {
        }

        public static OpenAddAppMerchantQueryBuilder anOpenAddAppMerchantQuery() {
            return new OpenAddAppMerchantQueryBuilder();
        }

        public OpenAddAppMerchantQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public OpenAddAppMerchantQueryBuilder thirdpartyId(String thirdpartyId) {
            this.thirdparty_id = thirdpartyId;
            return this;
        }

        public OpenAddAppMerchantQueryBuilder urlType(Integer urlType) {
            this.url_type = urlType;
            return this;
        }

        public OpenAddAppMerchantQueryBuilder transactionMerchantTokenType(TransactionMerchantTokenTypeEnum transactionMerchantTokenType) {
            this.transactionAccessTokenType = transactionMerchantTokenType;
            return this;
        }

        public OpenAddAppMerchantQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public OpenAddAppMerchantQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public OpenAddAppMerchantMerchantQuery build() {
            OpenAddAppMerchantMerchantQuery openAddAppMerchantQuery = new OpenAddAppMerchantMerchantQuery();
            openAddAppMerchantQuery.setApp_id(app_id);
            openAddAppMerchantQuery.setThirdparty_id(thirdparty_id);
            openAddAppMerchantQuery.setUrl_type(url_type);
            openAddAppMerchantQuery.setTransactionMerchantTokenType(transactionAccessTokenType);
            openAddAppMerchantQuery.setTenantId(tenantId);
            openAddAppMerchantQuery.setClientKey(clientKey);
            return openAddAppMerchantQuery;
        }
    }
}
