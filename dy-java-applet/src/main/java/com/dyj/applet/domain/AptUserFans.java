package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-24 14:08
 **/
public class AptUserFans {

    /**
     * 每天新粉丝数
     */
    private Long new_fans;

    /**
     * 每日总粉丝数
     */
    private Long total_fans;

    /**
     * 日期 yyyy-MM-dd
     */
    private String date;

    public Long getNew_fans() {
        return new_fans;
    }

    public void setNew_fans(Long new_fans) {
        this.new_fans = new_fans;
    }

    public Long getTotal_fans() {
        return total_fans;
    }

    public void setTotal_fans(Long total_fans) {
        this.total_fans = total_fans;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
