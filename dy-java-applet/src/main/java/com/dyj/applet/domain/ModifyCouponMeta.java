package com.dyj.applet.domain;

/**
 * 修改券模板请求值
 * @author ws
 **/
public class ModifyCouponMeta {

    /**
     * <p>使用须知（长度15以内），优惠券使用描述文案，用于在卡券包，直播领券等位置对优惠券的解释说明</p><p><br></p><p><strong>允许修改时机：</strong></p><p>主播创建发放计划前</p> 选填
     */
    private String consume_desc;
    /**
     * <p>当前券模版的去使用链接，用户领取后可在直播间、卡包等场景点击「去使用」后跳转到「该链接」</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">只能传path和query，不能以"/"开头</li></ul><p><br></p><p><strong>允许修改时机：</strong></p><p>主播创建发放计划前</p> 选填
     */
    private String consume_path;
    /**
     * <p>生效中的抖音开放平台小程序券模板ID</p>
     */
    private String coupon_meta_id;
    /**
     * <p><strong>券名称</strong>，长度15以内（汉字、英文、符号分别算一个字），不能包含敏感字符和特殊字符，若触发敏感字符接口会有错误码提示</p><p><strong>允许修改时机：</strong></p><p>主播创建发放计划前</p> 选填
     */
    private String coupon_name;
    /**
     * <p>券模板生效时间（即可领取开始时间），单位秒，和当前时间最大时间差为30天，小于当前时间，立即生效。</p><p><strong>该字段需要跟receive_end_time同时更改</strong></p><p><br></p><p><strong>允许修改时机：</strong></p><p>主播创建发放计划后</p> 选填
     */
    private Long receive_begin_time;
    /**
     * <p>券的领取须知（长度20以内），用于在卡包等场景展示当前优惠券的领取规则说明</p><p><br></p><p><strong>允许修改时机：</strong></p><p>主播创建发放计划前</p> 选填
     */
    private String receive_desc;
    /**
     * <p>券模板过期时间（即可领取结束时间），单位秒，必须大于receive_begin_time，大于当前时间，和当前时间最大时间差为60天</p><p><strong>该字段需要跟receive_begin_time同时更改</strong></p><p><br></p><p><strong>允许修改时机：</strong></p><p>主播创建发放计划后</p> 选填
     */
    private Long receive_end_time;
    /**
     * <p>用户领取的「券记录」的<strong>有效期开始时间</strong>（单位秒）：</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">当valid_type=1时，必填</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">和当前时间最大时间差为30天（即最大相差值为2592000）</li></ul><p><br></p><p><strong>允许修改时机：</strong></p><p>主播创建发放计划后</p> 选填
     */
    private Long valid_begin_time;
    /**
     * <p>用户领取的「券记录」的<strong>有效时长</strong>（单位秒）：</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">当valid_type=2时，必填</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">最大日期为30天（即最大值为2592000）</li></ul><p><br></p><p><strong>允许修改时机：</strong></p><p>主播创建发放计划后</p> 选填
     */
    private Long valid_duration;
    /**
     * <p>用户领取的「券记录」的<strong>有效期结束时间</strong>（单位秒）：</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">当valid_type=1时，必填</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">valid_begin_time必须大于满足：</li><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">大于当前时间</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">与当前时间最大时间差为60天</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">与valid_begin_time最大时间差为30天</li></ul></ul><p><br></p><p><strong>允许修改时机：</strong></p><p>主播创建发放计划后</p> 选填
     */
    private Long valid_end_time;
    /**
     * <p>用户领券后「该用户所领取的券记录」的<strong>有效期类型</strong>：</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">取值 1：固定时间，所有用户领券有效期一致</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">取值 2：领取后多少时间内有效</li></ul><p><br></p><p><strong>允许修改时机：</strong></p><p>主播创建发放计划后</p> 选填
     */
    private Integer valid_type;

    public String getConsume_desc() {
        return consume_desc;
    }

    public ModifyCouponMeta setConsume_desc(String consume_desc) {
        this.consume_desc = consume_desc;
        return this;
    }

    public String getConsume_path() {
        return consume_path;
    }

    public ModifyCouponMeta setConsume_path(String consume_path) {
        this.consume_path = consume_path;
        return this;
    }

    public String getCoupon_meta_id() {
        return coupon_meta_id;
    }

    public ModifyCouponMeta setCoupon_meta_id(String coupon_meta_id) {
        this.coupon_meta_id = coupon_meta_id;
        return this;
    }

    public String getCoupon_name() {
        return coupon_name;
    }

    public ModifyCouponMeta setCoupon_name(String coupon_name) {
        this.coupon_name = coupon_name;
        return this;
    }

    public Long getReceive_begin_time() {
        return receive_begin_time;
    }

    public ModifyCouponMeta setReceive_begin_time(Long receive_begin_time) {
        this.receive_begin_time = receive_begin_time;
        return this;
    }

    public String getReceive_desc() {
        return receive_desc;
    }

    public ModifyCouponMeta setReceive_desc(String receive_desc) {
        this.receive_desc = receive_desc;
        return this;
    }

    public Long getReceive_end_time() {
        return receive_end_time;
    }

    public ModifyCouponMeta setReceive_end_time(Long receive_end_time) {
        this.receive_end_time = receive_end_time;
        return this;
    }

    public Long getValid_begin_time() {
        return valid_begin_time;
    }

    public ModifyCouponMeta setValid_begin_time(Long valid_begin_time) {
        this.valid_begin_time = valid_begin_time;
        return this;
    }

    public Long getValid_duration() {
        return valid_duration;
    }

    public ModifyCouponMeta setValid_duration(Long valid_duration) {
        this.valid_duration = valid_duration;
        return this;
    }

    public Long getValid_end_time() {
        return valid_end_time;
    }

    public ModifyCouponMeta setValid_end_time(Long valid_end_time) {
        this.valid_end_time = valid_end_time;
        return this;
    }

    public Integer getValid_type() {
        return valid_type;
    }

    public ModifyCouponMeta setValid_type(Integer valid_type) {
        this.valid_type = valid_type;
        return this;
    }

    public static ModifyCouponMetaBuilder builder(){
        return new ModifyCouponMetaBuilder();
    }

    public static final class ModifyCouponMetaBuilder {
        private String consume_desc;
        private String consume_path;
        private String coupon_meta_id;
        private String coupon_name;
        private Long receive_begin_time;
        private String receive_desc;
        private Long receive_end_time;
        private Long valid_begin_time;
        private Long valid_duration;
        private Long valid_end_time;
        private Integer valid_type;

        private ModifyCouponMetaBuilder() {
        }

        public ModifyCouponMetaBuilder consumeDesc(String consumeDesc) {
            this.consume_desc = consumeDesc;
            return this;
        }

        public ModifyCouponMetaBuilder consumePath(String consumePath) {
            this.consume_path = consumePath;
            return this;
        }

        public ModifyCouponMetaBuilder couponMetaId(String couponMetaId) {
            this.coupon_meta_id = couponMetaId;
            return this;
        }

        public ModifyCouponMetaBuilder couponName(String couponName) {
            this.coupon_name = couponName;
            return this;
        }

        public ModifyCouponMetaBuilder receiveBeginTime(Long receiveBeginTime) {
            this.receive_begin_time = receiveBeginTime;
            return this;
        }

        public ModifyCouponMetaBuilder receiveDesc(String receiveDesc) {
            this.receive_desc = receiveDesc;
            return this;
        }

        public ModifyCouponMetaBuilder receiveEndTime(Long receiveEndTime) {
            this.receive_end_time = receiveEndTime;
            return this;
        }

        public ModifyCouponMetaBuilder validBeginTime(Long validBeginTime) {
            this.valid_begin_time = validBeginTime;
            return this;
        }

        public ModifyCouponMetaBuilder validDuration(Long validDuration) {
            this.valid_duration = validDuration;
            return this;
        }

        public ModifyCouponMetaBuilder validEndTime(Long validEndTime) {
            this.valid_end_time = validEndTime;
            return this;
        }

        public ModifyCouponMetaBuilder validType(Integer validType) {
            this.valid_type = validType;
            return this;
        }

        public ModifyCouponMeta build() {
            ModifyCouponMeta modifyCouponMeta = new ModifyCouponMeta();
            modifyCouponMeta.setConsume_desc(consume_desc);
            modifyCouponMeta.setConsume_path(consume_path);
            modifyCouponMeta.setCoupon_meta_id(coupon_meta_id);
            modifyCouponMeta.setCoupon_name(coupon_name);
            modifyCouponMeta.setReceive_begin_time(receive_begin_time);
            modifyCouponMeta.setReceive_desc(receive_desc);
            modifyCouponMeta.setReceive_end_time(receive_end_time);
            modifyCouponMeta.setValid_begin_time(valid_begin_time);
            modifyCouponMeta.setValid_duration(valid_duration);
            modifyCouponMeta.setValid_end_time(valid_end_time);
            modifyCouponMeta.setValid_type(valid_type);
            return modifyCouponMeta;
        }
    }
}
