package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 查询订单信息请求值
 */
public class QueryTransactionOrderQuery extends BaseQuery {

    /**
     * <p>交易订单号，order_id 与 out_order_no 二选一</p> 选填
     */
    private String order_id;
    /**
     * <p>开发者的单号，order_id 与 out_order_no 二选一</p> 选填
     */
    private String out_order_no;

    public String getOrder_id() {
        return order_id;
    }

    public QueryTransactionOrderQuery setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getOut_order_no() {
        return out_order_no;
    }

    public QueryTransactionOrderQuery setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public static QueryOrderQueryBuilder builder() {
        return new QueryOrderQueryBuilder();
    }


    public static final class QueryOrderQueryBuilder {
        private String order_id;
        private String out_order_no;
        private Integer tenantId;
        private String clientKey;

        private QueryOrderQueryBuilder() {
        }


        public QueryOrderQueryBuilder orderId(String orderId) {
            this.order_id = orderId;
            return this;
        }

        public QueryOrderQueryBuilder outOrderNo(String outOrderNo) {
            this.out_order_no = outOrderNo;
            return this;
        }

        public QueryOrderQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QueryOrderQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryTransactionOrderQuery build() {
            QueryTransactionOrderQuery queryTransactionOrderQuery = new QueryTransactionOrderQuery();
            queryTransactionOrderQuery.setOrder_id(order_id);
            queryTransactionOrderQuery.setOut_order_no(out_order_no);
            queryTransactionOrderQuery.setTenantId(tenantId);
            queryTransactionOrderQuery.setClientKey(clientKey);
            return queryTransactionOrderQuery;
        }
    }
}
