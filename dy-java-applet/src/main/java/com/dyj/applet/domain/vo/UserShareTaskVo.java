package com.dyj.applet.domain.vo;

/**
 * @author danmo
 * @date 2024-05-28 14:48
 **/
public class UserShareTaskVo {

    /**
     * 任务id
     */
    private String task_id;
    /**
     * 任务目标分享次数
     */
    private Long target_count;
    /**
     * 用户已分享成功次数
     */
    private Long success_count;
    /**
     * 是否有效
     */
    private Boolean is_valid;

    /**
     * 用户是否完成任务
     */
    private Boolean completed;

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public Long getTarget_count() {
        return target_count;
    }

    public void setTarget_count(Long target_count) {
        this.target_count = target_count;
    }

    public Long getSuccess_count() {
        return success_count;
    }

    public void setSuccess_count(Long success_count) {
        this.success_count = success_count;
    }

    public Boolean getIs_valid() {
        return is_valid;
    }

    public void setIs_valid(Boolean is_valid) {
        this.is_valid = is_valid;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }
}
