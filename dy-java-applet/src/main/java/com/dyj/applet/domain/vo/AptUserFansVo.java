package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AptUserFans;
import com.dyj.common.domain.vo.BaseVo;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-24 14:07
 **/
public class AptUserFansVo extends BaseVo {

    private List<AptUserFans> result_list;

    public List<AptUserFans> getResult_list() {
        return result_list;
    }

    public void setResult_list(List<AptUserFans> result_list) {
        this.result_list = result_list;
    }
}
