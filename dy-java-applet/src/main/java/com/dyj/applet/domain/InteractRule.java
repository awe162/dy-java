package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-28 14:23
 **/
public class InteractRule {

    /**
     * 目标值，上限为500
     */
    private Integer target_count;
    /**
     * 单个阶段的数值，下限为5，上限为100
     */
    private Integer stage_count;

    public Integer getTarget_count() {
        return target_count;
    }

    public void setTarget_count(Integer target_count) {
        this.target_count = target_count;
    }

    public Integer getStage_count() {
        return stage_count;
    }

    public void setStage_count(Integer stage_count) {
        this.stage_count = stage_count;
    }
}
