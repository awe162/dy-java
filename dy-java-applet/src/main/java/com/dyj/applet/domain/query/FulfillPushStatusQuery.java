package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

/**
 * 推送履约状态查询值
 */
public class FulfillPushStatusQuery extends BaseQuery {

    /**
     * 通用交易系统侧订单号，长度 <= 64 byte
     */
    private String order_id;

    /**
     * 通用交易系统item单号，每个单号的长度 <= 64 byte
     * 注：可通过订单查询接口获得
     */
    private List<String> item_order_id_list;

    /**
     * 普通咨询
     * fulfill_done 履约完成 用于服务结束后，将履约状态由“履约中”驱动到“履约完成”
     * 号卡商品
     * fulfilling 履约中 用于商家制卡发货后，将履约状态驱动到“履约中”
     * fulfill_done 履约完成 用于买家收货后，将履约状态由“履约中”驱动到“履约完成”
     * 定制服务
     * fulfilling 履约中 用于服务定制中，将履约状态驱动到“履约中”
     * fulfill_done 履约完成 用于定制交付后，将履约状态由“履约中”驱动到“履约完成”
     */
    private String to_status;

    public String getOrder_id() {
        return order_id;
    }

    public FulfillPushStatusQuery setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public List<String> getItem_order_id_list() {
        return item_order_id_list;
    }

    public FulfillPushStatusQuery setItem_order_id_list(List<String> item_order_id_list) {
        this.item_order_id_list = item_order_id_list;
        return this;
    }

    public String getTo_status() {
        return to_status;
    }

    public FulfillPushStatusQuery setTo_status(String to_status) {
        this.to_status = to_status;
        return this;
    }

    public static FulfillPushStatusQueryBuilder builder() {
        return new FulfillPushStatusQueryBuilder();
    }

    public static final class FulfillPushStatusQueryBuilder {
        private String order_id;
        private List<String> item_order_id_list;
        private String to_status;
        private Integer tenantId;
        private String clientKey;

        private FulfillPushStatusQueryBuilder() {
        }

        public FulfillPushStatusQueryBuilder orderId(String orderId) {
            this.order_id = orderId;
            return this;
        }

        public FulfillPushStatusQueryBuilder itemOrderIdList(List<String> itemOrderIdList) {
            this.item_order_id_list = itemOrderIdList;
            return this;
        }

        public FulfillPushStatusQueryBuilder toStatus(String toStatus) {
            this.to_status = toStatus;
            return this;
        }

        public FulfillPushStatusQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public FulfillPushStatusQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public FulfillPushStatusQuery build() {
            FulfillPushStatusQuery fulfillPushStatusQuery = new FulfillPushStatusQuery();
            fulfillPushStatusQuery.setOrder_id(order_id);
            fulfillPushStatusQuery.setItem_order_id_list(item_order_id_list);
            fulfillPushStatusQuery.setTo_status(to_status);
            fulfillPushStatusQuery.setTenantId(tenantId);
            fulfillPushStatusQuery.setClientKey(clientKey);
            return fulfillPushStatusQuery;
        }
    }
}
