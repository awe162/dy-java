package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-24 14:23
 **/
public class AptUserProfile {

    private Long profile_uv;

    private String date;

    public Long getProfile_uv() {
        return profile_uv;
    }

    public void setProfile_uv(Long profile_uv) {
        this.profile_uv = profile_uv;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
