package com.dyj.applet.domain;

/**
 * 标签组对应规则信息
 */
public class TagQueryTagDetailRule {

    /**
     * <p>规则id</p>
     */
    private String rule_id;
    /**
     * <p>规则类型，枚举值：</p><p>1：退款规则</p>
     */
    private Integer rule_type;
    /**
     * <p>退款规则，rule_type为1是必填</p> 选填
     */
    private TagQueryTagDetailRuleRefundRule refund_rule;

    public String getRule_id() {
        return rule_id;
    }

    public TagQueryTagDetailRule setRule_id(String rule_id) {
        this.rule_id = rule_id;
        return this;
    }

    public Integer getRule_type() {
        return rule_type;
    }

    public TagQueryTagDetailRule setRule_type(Integer rule_type) {
        this.rule_type = rule_type;
        return this;
    }

    public TagQueryTagDetailRuleRefundRule getRefund_rule() {
        return refund_rule;
    }

    public TagQueryTagDetailRule setRefund_rule(TagQueryTagDetailRuleRefundRule refund_rule) {
        this.refund_rule = refund_rule;
        return this;
    }
}
