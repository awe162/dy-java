package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 删除开发者接口发券活动请求值
 */
public class DeleteDeveloperActivityQuery extends BaseQuery {

    /**
     * 活动id
     */
    private String activity_id;
    /**
     * appId
     */
    private String app_id;

    public String getActivity_id() {
        return activity_id;
    }

    public DeleteDeveloperActivityQuery setActivity_id(String activity_id) {
        this.activity_id = activity_id;
        return this;
    }

    public String getApp_id() {
        return app_id;
    }

    public DeleteDeveloperActivityQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public static DeleteDeveloperActivityQueryBuilder builder(){
        return new DeleteDeveloperActivityQueryBuilder();
    }


    public static final class DeleteDeveloperActivityQueryBuilder {
        private String activity_id;
        private String app_id;
        private Integer tenantId;
        private String clientKey;

        private DeleteDeveloperActivityQueryBuilder() {
        }

        public DeleteDeveloperActivityQueryBuilder activityId(String activityId) {
            this.activity_id = activityId;
            return this;
        }

        public DeleteDeveloperActivityQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public DeleteDeveloperActivityQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public DeleteDeveloperActivityQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public DeleteDeveloperActivityQuery build() {
            DeleteDeveloperActivityQuery deleteDeveloperActivityQuery = new DeleteDeveloperActivityQuery();
            deleteDeveloperActivityQuery.setActivity_id(activity_id);
            deleteDeveloperActivityQuery.setApp_id(app_id);
            deleteDeveloperActivityQuery.setTenantId(tenantId);
            deleteDeveloperActivityQuery.setClientKey(clientKey);
            return deleteDeveloperActivityQuery;
        }
    }
}
