package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-06 18:31
 **/
public class CommonPlanTalentSellDetail {
    /**
     * 带货GMV，单位分
     */
    private Long gmv;
    /**
     *直播间数量
     */
    private Integer live_cnt;
    /**
     * 短视频数量
     */
    private Integer short_video_cnt;
    /**
     * 达人佣金，单位分
     */
    private Long talent_commission;
    /**
     * 已核销GMV，单位分
     */
    private Long used_gmv;

    public Long getGmv() {
        return gmv;
    }

    public void setGmv(Long gmv) {
        this.gmv = gmv;
    }

    public Integer getLive_cnt() {
        return live_cnt;
    }

    public void setLive_cnt(Integer live_cnt) {
        this.live_cnt = live_cnt;
    }

    public Integer getShort_video_cnt() {
        return short_video_cnt;
    }

    public void setShort_video_cnt(Integer short_video_cnt) {
        this.short_video_cnt = short_video_cnt;
    }

    public Long getTalent_commission() {
        return talent_commission;
    }

    public void setTalent_commission(Long talent_commission) {
        this.talent_commission = talent_commission;
    }

    public Long getUsed_gmv() {
        return used_gmv;
    }

    public void setUsed_gmv(Long used_gmv) {
        this.used_gmv = used_gmv;
    }
}
