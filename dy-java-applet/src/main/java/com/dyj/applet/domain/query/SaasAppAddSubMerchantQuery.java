package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseTransactionMerchantQuery;
import com.dyj.common.enums.TransactionMerchantTokenTypeEnum;

/**
 * 开发者获取小程序收款商户/合作方提现页面查询条件
 */
public class SaasAppAddSubMerchantQuery extends BaseTransactionMerchantQuery {

    /**
     * 商户 id，用于接入方自行标识并管理进件方。由开发者自行分配管理
     */
    public String sub_merchant_id;

    /**
     * 小程序的 app_id
     */
    private String app_id;

    /**
     * 链接类型枚举值：
     * 2: 账户余额页（可以发起提现）
     * 3: 提现记录页
     */
    private Integer url_type;

    /**
     * 1：收款商户
     * 2：商户的合作方
     * 不传默认为2
     */
    private Integer role;

    public String getSub_merchant_id() {
        return sub_merchant_id;
    }

    public SaasAppAddSubMerchantQuery setSub_merchant_id(String sub_merchant_id) {
        this.sub_merchant_id = sub_merchant_id;
        return this;
    }

    public String getApp_id() {
        return app_id;
    }

    public SaasAppAddSubMerchantQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public Integer getUrl_type() {
        return url_type;
    }

    public SaasAppAddSubMerchantQuery setUrl_type(Integer url_type) {
        this.url_type = url_type;
        return this;
    }

    public Integer getRole() {
        return role;
    }

    public SaasAppAddSubMerchantQuery setRole(Integer role) {
        this.role = role;
        return this;
    }

    public static SaasAppAddSubMerchantQueryBuilder builder() {
        return new SaasAppAddSubMerchantQueryBuilder();
    }

    public static final class SaasAppAddSubMerchantQueryBuilder {
        private String sub_merchant_id;
        private String app_id;
        private Integer url_type;
        private Integer role;
        private TransactionMerchantTokenTypeEnum transactionMerchantTokenType;
        private Integer tenantId;
        private String clientKey;

        private SaasAppAddSubMerchantQueryBuilder() {
        }

        public SaasAppAddSubMerchantQueryBuilder subMerchantId(String subMerchantId) {
            this.sub_merchant_id = subMerchantId;
            return this;
        }

        public SaasAppAddSubMerchantQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public SaasAppAddSubMerchantQueryBuilder urlType(Integer urlType) {
            this.url_type = urlType;
            return this;
        }

        public SaasAppAddSubMerchantQueryBuilder role(Integer role) {
            this.role = role;
            return this;
        }

        public SaasAppAddSubMerchantQueryBuilder transactionMerchantTokenType(TransactionMerchantTokenTypeEnum transactionMerchantTokenType) {
            this.transactionMerchantTokenType = transactionMerchantTokenType;
            return this;
        }

        public SaasAppAddSubMerchantQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public SaasAppAddSubMerchantQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public SaasAppAddSubMerchantQuery build() {
            SaasAppAddSubMerchantQuery saasAppAddSubMerchantQuery = new SaasAppAddSubMerchantQuery();
            saasAppAddSubMerchantQuery.setSub_merchant_id(sub_merchant_id);
            saasAppAddSubMerchantQuery.setApp_id(app_id);
            saasAppAddSubMerchantQuery.setUrl_type(url_type);
            saasAppAddSubMerchantQuery.setRole(role);
            saasAppAddSubMerchantQuery.setTransactionMerchantTokenType(transactionMerchantTokenType);
            saasAppAddSubMerchantQuery.setTenantId(tenantId);
            saasAppAddSubMerchantQuery.setClientKey(clientKey);
            return saasAppAddSubMerchantQuery;
        }
    }
}
