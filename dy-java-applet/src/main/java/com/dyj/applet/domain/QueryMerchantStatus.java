package com.dyj.applet.domain;

import java.util.Map;

/**
 * 进件查询返回值
 */
public class QueryMerchantStatus {


    /**
     * <p>抖音信息主体渠道进件信息，以渠道名为key，<strong>渠道进件信息</strong>作为value</p>
     */
    private Map<String,QueryMerchantStatusStatusInfo> merchant_status_info;
    /**
     * <p>光合信号主体渠道进件信息以渠道名为key，<strong>渠道进件信息</strong>为value</p>
     */
    private Map<String,QueryMerchantStatusStatusInfo> new_merchant_status_info;
    /**
     * 进件状态信息 选填
     */
    private Map<String,QueryMerchantStatusStatusInfo> pay_status_info;

    public Map<String, QueryMerchantStatusStatusInfo> getMerchant_status_info() {
        return merchant_status_info;
    }

    public QueryMerchantStatus setMerchant_status_info(Map<String, QueryMerchantStatusStatusInfo> merchant_status_info) {
        this.merchant_status_info = merchant_status_info;
        return this;
    }

    public Map<String, QueryMerchantStatusStatusInfo> getNew_merchant_status_info() {
        return new_merchant_status_info;
    }

    public QueryMerchantStatus setNew_merchant_status_info(Map<String, QueryMerchantStatusStatusInfo> new_merchant_status_info) {
        this.new_merchant_status_info = new_merchant_status_info;
        return this;
    }

    public Map<String, QueryMerchantStatusStatusInfo> getPay_status_info() {
        return pay_status_info;
    }

    public QueryMerchantStatus setPay_status_info(Map<String, QueryMerchantStatusStatusInfo> pay_status_info) {
        this.pay_status_info = pay_status_info;
        return this;
    }
}
