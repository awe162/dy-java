package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-21 10:23
 **/
public class PlayletBusinessUploadContext {

    private PlayletBusinessUploadContextDevice device;

    private PlayletBusinessUploadContextAd ad;


    public PlayletBusinessUploadContextDevice getDevice() {
        return device;
    }

    public void setDevice(PlayletBusinessUploadContextDevice device) {
        this.device = device;
    }

    public PlayletBusinessUploadContextAd getAd() {
        return ad;
    }

    public void setAd(PlayletBusinessUploadContextAd ad) {
        this.ad = ad;
    }
}
