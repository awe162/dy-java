package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * @author danmo
 * @date 2024-06-17 15:19
 **/
public class OpenTrafficPermissionQuery extends BaseQuery {

    /**
     * 银行账号
     */
    private String bank_account_number;
    /**
     * 开户支行名称
     */
    private String bank_branch;
    /**
     * 开户银行名称
     */
    private String bank_name;
    /**
     * 城市
     */
    private String city;
    /**
     * 手机号码
     */
    private String phone_number;
    /**
     * 省份
     */
    private String province;
    /**
     * 纳税人性质
     * 1：一般纳税人
     * 2：小规模纳税人
     */
    private Integer tax_nature;


    public static OpenTrafficPermissionQueryBuilder builder() {
        return new OpenTrafficPermissionQueryBuilder();
    }

    public static class OpenTrafficPermissionQueryBuilder {
        /**
         * 银行账号
         */
        private String bankAccountNumber;
        /**
         * 开户支行名称
         */
        private String bankBranch;
        /**
         * 开户银行名称
         */
        private String bankName;
        /**
         * 城市
         */
        private String city;
        /**
         * 手机号码
         */
        private String phoneNumber;
        /**
         * 省份
         */
        private String province;
        /**
         * 纳税人性质
         * 1：一般纳税人
         * 2：小规模纳税人
         */
        private Integer taxNature;

        /**
         * 租户ID
         */
        protected Integer tenantId;
        /**
         * 应用Key
         */
        protected String clientKey;

        public OpenTrafficPermissionQueryBuilder bankAccountNumber(String bankAccountNumber) {
            this.bankAccountNumber = bankAccountNumber;
            return this;
        }

        public OpenTrafficPermissionQueryBuilder bankBranch(String bankBranch) {
            this.bankBranch = bankBranch;
            return this;
        }

        public OpenTrafficPermissionQueryBuilder bankName(String bankName) {
            this.bankName = bankName;
            return this;
        }

        public OpenTrafficPermissionQueryBuilder city(String city) {
            this.city = city;
            return this;
        }

        public OpenTrafficPermissionQueryBuilder phoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public OpenTrafficPermissionQueryBuilder province(String province) {
            this.province = province;
            return this;
        }

        public OpenTrafficPermissionQueryBuilder taxNature(Integer taxNature) {
            this.taxNature = taxNature;
            return this;
        }

        public OpenTrafficPermissionQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public OpenTrafficPermissionQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public OpenTrafficPermissionQuery build() {
            OpenTrafficPermissionQuery openTrafficPermissionQuery = new OpenTrafficPermissionQuery();
            openTrafficPermissionQuery.setBank_account_number(bankAccountNumber);
            openTrafficPermissionQuery.setBank_branch(bankBranch);
            openTrafficPermissionQuery.setBank_name(bankName);
            openTrafficPermissionQuery.setCity(city);
            openTrafficPermissionQuery.setPhone_number(phoneNumber);
            openTrafficPermissionQuery.setProvince(province);
            openTrafficPermissionQuery.setTax_nature(taxNature);
            openTrafficPermissionQuery.setTenantId(tenantId);
            openTrafficPermissionQuery.setClientKey(clientKey);
            return openTrafficPermissionQuery;
        }


    }

    public String getBank_account_number() {
        return bank_account_number;
    }

    public void setBank_account_number(String bank_account_number) {
        this.bank_account_number = bank_account_number;
    }

    public String getBank_branch() {
        return bank_branch;
    }

    public void setBank_branch(String bank_branch) {
        this.bank_branch = bank_branch;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public Integer getTax_nature() {
        return tax_nature;
    }

    public void setTax_nature(Integer tax_nature) {
        this.tax_nature = tax_nature;
    }
}
