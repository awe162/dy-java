package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;
import com.dyj.common.domain.query.BaseTransactionMerchantQuery;
import com.dyj.common.enums.TransactionMerchantTokenTypeEnum;

/**
 * 商户余额查询请求值
 */
public class QueryChannelBalanceAccountQuery extends BaseTransactionMerchantQuery {

    /**
     * <p>小程序的 app_id。</p><p>在服务商为自己提现的情况下可不填，其他情况必填</p> 选填
     */
    private String app_id;
    /**
     * <p>提现渠道枚举值:</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">alipay: 担保支付普通版支付宝</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">wx: 担保支付普通版微信</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">hz: 担保支付普通版抖音支付</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">yzt: 担保支付企业版聚合账户</li></ul>
     */
    private String channel_type;
    /**
     * <p>抖音信息和光合信号主体标识:</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">不传或传0或1 查<a href="https://developer.open-douyin.com/docs/resource/zh-CN/mini-app/open-capacity/guaranteed-payment/FAQ/change" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">抖音信息主体</a>账户余额</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">传2查<a href="https://developer.open-douyin.com/docs/resource/zh-CN/mini-app/open-capacity/guaranteed-payment/FAQ/change" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">光合信号主体</a>账户余额</li></ul><p>说明：channel_type为yzt时，指定为2</p> 选填
     */
    private Integer merchant_entity;
    /**
     * <p>进件完成返回的商户号</p>
     */
    private String merchant_uid;
    /**
     * <p>小程序第三方平台应用 id。</p><p>在服务商发起提现请求的条件下必填</p> 选填
     */
    private String thirdparty_id;


    public String getApp_id() {
        return app_id;
    }

    public QueryChannelBalanceAccountQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getChannel_type() {
        return channel_type;
    }

    public QueryChannelBalanceAccountQuery setChannel_type(String channel_type) {
        this.channel_type = channel_type;
        return this;
    }

    public Integer getMerchant_entity() {
        return merchant_entity;
    }

    public QueryChannelBalanceAccountQuery setMerchant_entity(Integer merchant_entity) {
        this.merchant_entity = merchant_entity;
        return this;
    }

    public String getMerchant_uid() {
        return merchant_uid;
    }

    public QueryChannelBalanceAccountQuery setMerchant_uid(String merchant_uid) {
        this.merchant_uid = merchant_uid;
        return this;
    }

    public String getThirdparty_id() {
        return thirdparty_id;
    }

    public QueryChannelBalanceAccountQuery setThirdparty_id(String thirdparty_id) {
        this.thirdparty_id = thirdparty_id;
        return this;
    }

    public static QueryChannelBalanceAccountQueryBuilder builder() {
        return new QueryChannelBalanceAccountQueryBuilder();
    }

    public static final class QueryChannelBalanceAccountQueryBuilder {
        private String app_id;
        private String channel_type;
        private Integer merchant_entity;
        private String merchant_uid;
        private String thirdparty_id;
        private Integer tenantId;
        private String clientKey;
        private TransactionMerchantTokenTypeEnum transactionMerchantTokenType;

        private QueryChannelBalanceAccountQueryBuilder() {
        }

        public QueryChannelBalanceAccountQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public QueryChannelBalanceAccountQueryBuilder channelType(String channelType) {
            this.channel_type = channelType;
            return this;
        }

        public QueryChannelBalanceAccountQueryBuilder merchantEntity(Integer merchantEntity) {
            this.merchant_entity = merchantEntity;
            return this;
        }

        public QueryChannelBalanceAccountQueryBuilder merchantUid(String merchantUid) {
            this.merchant_uid = merchantUid;
            return this;
        }

        public QueryChannelBalanceAccountQueryBuilder thirdpartyId(String thirdpartyId) {
            this.thirdparty_id = thirdpartyId;
            return this;
        }

        public QueryChannelBalanceAccountQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QueryChannelBalanceAccountQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryChannelBalanceAccountQueryBuilder transactionMerchantTokenType(TransactionMerchantTokenTypeEnum transactionMerchantTokenType){
            this.transactionMerchantTokenType = transactionMerchantTokenType;
            return this;
        }

        public QueryChannelBalanceAccountQuery build() {
            QueryChannelBalanceAccountQuery queryChannelBalanceAccountQuery = new QueryChannelBalanceAccountQuery();
            queryChannelBalanceAccountQuery.setApp_id(app_id);
            queryChannelBalanceAccountQuery.setChannel_type(channel_type);
            queryChannelBalanceAccountQuery.setMerchant_entity(merchant_entity);
            queryChannelBalanceAccountQuery.setMerchant_uid(merchant_uid);
            queryChannelBalanceAccountQuery.setThirdparty_id(thirdparty_id);
            queryChannelBalanceAccountQuery.setTenantId(tenantId);
            queryChannelBalanceAccountQuery.setClientKey(clientKey);
            queryChannelBalanceAccountQuery.setTransactionMerchantTokenType(transactionMerchantTokenType);
            return queryChannelBalanceAccountQuery;
        }
    }
}
