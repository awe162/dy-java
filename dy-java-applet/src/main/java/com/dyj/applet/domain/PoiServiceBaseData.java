package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-06 15:44
 **/
public class PoiServiceBaseData {

    /**
     * 日期	yyyy-MM-dd
     */
    private String date;
    /**
     * 曝光uv
     */
    private Long exposure_uv;
    /**
     * 订房成交次数
     */
    private Long success_order_cnt;
    /**
     * 点击uv
     */
    private Long click_uv;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getExposure_uv() {
        return exposure_uv;
    }

    public void setExposure_uv(Long exposure_uv) {
        this.exposure_uv = exposure_uv;
    }

    public Long getSuccess_order_cnt() {
        return success_order_cnt;
    }

    public void setSuccess_order_cnt(Long success_order_cnt) {
        this.success_order_cnt = success_order_cnt;
    }

    public Long getClick_uv() {
        return click_uv;
    }

    public void setClick_uv(Long click_uv) {
        this.click_uv = click_uv;
    }
}
