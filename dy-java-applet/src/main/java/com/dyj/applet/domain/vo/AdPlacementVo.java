package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AdPlacement;

import java.util.List;

/**
 * @author danmo
 * @date 2024-06-17 15:26
 **/
public class AdPlacementVo {

    private List<AdPlacement> ad_placement_list;

    public List<AdPlacement> getAd_placement_list() {
        return ad_placement_list;
    }

    public void setAd_placement_list(List<AdPlacement> ad_placement_list) {
        this.ad_placement_list = ad_placement_list;
    }
}
