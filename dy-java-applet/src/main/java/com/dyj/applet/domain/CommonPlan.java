package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-13 16:07
 **/
public class CommonPlan {

    /**
     * 计划ID
     */
    private Long plan_id;
    /**
     * 可选值：1：进行中2：暂停中 3：已终止
     */
    private Integer status;
    /**
     * 计划支持的带货场景，可选以下的值：1：仅短视频2：仅直播间3：短视频和直播间
     */
    private Integer content_type;
    /**
     * 分佣率，万分位
     */
    private Long commission_rate;
    /**
     * 计划创建时间
     */
    private String create_time;

    /**
     * 创建时间戳
     */
    private Long create_time_unix;

    /**
     * 更新时间
     */
    private String update_time;

    /**
     * 更新时间戳
     */
    private Long update_time_unix;


    public Long getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(Long plan_id) {
        this.plan_id = plan_id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getContent_type() {
        return content_type;
    }

    public void setContent_type(Integer content_type) {
        this.content_type = content_type;
    }

    public Long getCommission_rate() {
        return commission_rate;
    }

    public void setCommission_rate(Long commission_rate) {
        this.commission_rate = commission_rate;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public Long getCreate_time_unix() {
        return create_time_unix;
    }

    public void setCreate_time_unix(Long create_time_unix) {
        this.create_time_unix = create_time_unix;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public Long getUpdate_time_unix() {
        return update_time_unix;
    }

    public void setUpdate_time_unix(Long update_time_unix) {
        this.update_time_unix = update_time_unix;
    }
}
