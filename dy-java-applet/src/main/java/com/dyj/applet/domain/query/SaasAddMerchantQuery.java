package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseTransactionMerchantQuery;
import com.dyj.common.enums.TransactionMerchantTokenTypeEnum;

/**
 * 服务商获取服务商提现页面查询
 */
public class SaasAddMerchantQuery extends BaseTransactionMerchantQuery {

    /**
     * 授权码兑换接口调用凭证
     */
    private String component_access_token;

    /**
     * 小程序第三方平台应用 id
     */
    private String thirdparty_component_id;

    /**
     * 链接类型枚举值：
     * 2: 账户余额页（可以发起提现）
     * 3: 提现记录页
     */
    private Integer url_type;

    /**
     * 商户号对应的支付解决方案
     * 1：担保支付｜普通版
     * 2：担保支付｜企业版
     * 不传默认为1
     */
    private Integer prod_id;

    public String getComponent_access_token() {
        return component_access_token;
    }

    public SaasAddMerchantQuery setComponent_access_token(String component_access_token) {
        this.component_access_token = component_access_token;
        return this;
    }

    public String getThirdparty_component_id() {
        return thirdparty_component_id;
    }

    public SaasAddMerchantQuery setThirdparty_component_id(String thirdparty_component_id) {
        this.thirdparty_component_id = thirdparty_component_id;
        return this;
    }

    public Integer getUrl_type() {
        return url_type;
    }

    public SaasAddMerchantQuery setUrl_type(Integer url_type) {
        this.url_type = url_type;
        return this;
    }

    public Integer getProd_id() {
        return prod_id;
    }

    public SaasAddMerchantQuery setProd_id(Integer prod_id) {
        this.prod_id = prod_id;
        return this;
    }

    public static SaasAddMerchantQueryBuilder builder() {
        return new SaasAddMerchantQueryBuilder();
    }

    public static final class SaasAddMerchantQueryBuilder {
        private String component_access_token;
        private String thirdparty_component_id;
        private Integer url_type;
        private Integer prod_id;
        private TransactionMerchantTokenTypeEnum transactionMerchantTokenType;
        private Integer tenantId;
        private String clientKey;

        private SaasAddMerchantQueryBuilder() {
        }

        public SaasAddMerchantQueryBuilder componentAccessToken(String componentAccessToken) {
            this.component_access_token = componentAccessToken;
            return this;
        }

        public SaasAddMerchantQueryBuilder thirdpartyComponentId(String thirdpartyComponentId) {
            this.thirdparty_component_id = thirdpartyComponentId;
            return this;
        }

        public SaasAddMerchantQueryBuilder urlType(Integer urlType) {
            this.url_type = urlType;
            return this;
        }

        public SaasAddMerchantQueryBuilder prodId(Integer prodId) {
            this.prod_id = prodId;
            return this;
        }

        public SaasAddMerchantQueryBuilder transactionMerchantTokenType(TransactionMerchantTokenTypeEnum transactionMerchantTokenType) {
            this.transactionMerchantTokenType = transactionMerchantTokenType;
            return this;
        }

        public SaasAddMerchantQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public SaasAddMerchantQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public SaasAddMerchantQuery build() {
            SaasAddMerchantQuery saasAddMerchantQuery = new SaasAddMerchantQuery();
            saasAddMerchantQuery.setComponent_access_token(component_access_token);
            saasAddMerchantQuery.setThirdparty_component_id(thirdparty_component_id);
            saasAddMerchantQuery.setUrl_type(url_type);
            saasAddMerchantQuery.setProd_id(prod_id);
            saasAddMerchantQuery.setTransactionMerchantTokenType(transactionMerchantTokenType);
            saasAddMerchantQuery.setTenantId(tenantId);
            saasAddMerchantQuery.setClientKey(clientKey);
            return saasAddMerchantQuery;
        }
    }
}
