package com.dyj.applet.domain;

import java.util.Map;

/**
 * 售卖单元
 * @author danmo
 * @date 2024-05-06 10:38
 **/
public class SkuStruct {

    /**
     * sku id，创建时不必填写
     */
    private String sku_id;
    /**
     * sku名
     */
    private String sku_name;
    /**
     * 原价，团购创建时如有commodity属性可不填，会根据菜品搭配计算原价
     */
    private Integer origin_amount;
    /**
     * 实际支付价格
     */
    private Integer actual_amount;
    /**
     * 库存信息
     */
    private StockStruct stock;
    /**
     * 第三方id
     */
    private String out_sku_id;
    /**
     * 状态。1：在线   -1-删除； 默认传1
     */
    private Integer status;
    /**
     * sku属性KV，填写时参考下文「attr_key_value_map的格式」
     */
    private Map<String, Object> attr_key_value_map;

    public static SkuStructBuilder builder() {
        return new SkuStructBuilder();
    }

    public static class SkuStructBuilder {
        private String skuId;
        private String skuName;
        private Integer originAmount;
        private Integer actualAmount;
        private StockStruct stock;
        private String outSkuId;
        private Integer status;
        private Map<String, Object> attrKeyValueMap;
        public SkuStructBuilder skuId(String skuId) {
            this.skuId = skuId;
            return this;
        }
        public SkuStructBuilder skuName(String skuName) {
            this.skuName = skuName;
            return this;
        }
        public SkuStructBuilder originAmount(Integer originAmount) {
            this.originAmount = originAmount;
            return this;
        }
        public SkuStructBuilder actualAmount(Integer actualAmount) {
            this.actualAmount = actualAmount;
            return this;
        }
        public SkuStructBuilder stock(StockStruct stock) {
            this.stock = stock;
            return this;
        }
        public SkuStructBuilder outSkuId(String outSkuId) {
            this.outSkuId = outSkuId;
            return this;
        }
        public SkuStructBuilder status(Integer status) {
            this.status = status;
            return this;
        }
        public SkuStructBuilder attrKeyValueMap(Map<String, Object> attrKeyValueMap) {
            this.attrKeyValueMap = attrKeyValueMap;
            return this;
        }
        public SkuStruct build() {
            SkuStruct skuStruct = new SkuStruct();
            skuStruct.setSku_id(skuId);
            skuStruct.setSku_name(skuName);
            skuStruct.setOrigin_amount(originAmount);
            skuStruct.setActual_amount(actualAmount);
            skuStruct.setStock(stock);
            skuStruct.setOut_sku_id(outSkuId);
            skuStruct.setStatus(status);
            skuStruct.setAttr_key_value_map(attrKeyValueMap);
            return skuStruct;
        }
}

    public String getSku_id() {
        return sku_id;
    }

    public void setSku_id(String sku_id) {
        this.sku_id = sku_id;
    }

    public String getSku_name() {
        return sku_name;
    }

    public void setSku_name(String sku_name) {
        this.sku_name = sku_name;
    }

    public Integer getOrigin_amount() {
        return origin_amount;
    }

    public void setOrigin_amount(Integer origin_amount) {
        this.origin_amount = origin_amount;
    }

    public Integer getActual_amount() {
        return actual_amount;
    }

    public void setActual_amount(Integer actual_amount) {
        this.actual_amount = actual_amount;
    }

    public StockStruct getStock() {
        return stock;
    }

    public void setStock(StockStruct stock) {
        this.stock = stock;
    }

    public String getOut_sku_id() {
        return out_sku_id;
    }

    public void setOut_sku_id(String out_sku_id) {
        this.out_sku_id = out_sku_id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Map<String, Object> getAttr_key_value_map() {
        return attr_key_value_map;
    }

    public void setAttr_key_value_map(Map<String, Object> attr_key_value_map) {
        this.attr_key_value_map = attr_key_value_map;
    }
}
