package com.dyj.applet.domain.vo;

/**
 * 商户提现结果查询返回值
 */
public class QueryWithdrawOrderVo {

    /**
     * <p>状态枚举值:</p><ul><li>成功:SUCCESS</li><li>失败: FAIL</li><li>处理中: PROCESSING</li><li>退票: REEXCHANGE</li></ul><p>注：退票：商户的提现申请请求通过渠道（微信/支付宝/抖音支付）提交给银行处理后，银行返回结果是处理成功，渠道返回给商户提现成功，但间隔一段时间后，银行再次通知渠道处理失败并返还款项给渠道，渠道再将该笔失败款返还至商户在渠道的账户余额中</p>
     */
    private String status;
    /**
     * <p>状态描述</p>
     */
    private String status_msg;

    public String getStatus() {
        return status;
    }

    public QueryWithdrawOrderVo setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getStatus_msg() {
        return status_msg;
    }

    public QueryWithdrawOrderVo setStatus_msg(String status_msg) {
        this.status_msg = status_msg;
        return this;
    }
}
