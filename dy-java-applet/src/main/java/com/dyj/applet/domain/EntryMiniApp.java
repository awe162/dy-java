package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-04-28 15:22
 **/
public class EntryMiniApp {

    /**
     * 服务参数json
     */
    private String params;
    /**
     * 服务路径
     */
    private String path;
    /**
     * 小程序的appid
     */
    private String app_id;

    public SupplierServiceEntryMiniAppBuilder builder() {
        return new SupplierServiceEntryMiniAppBuilder();
    }

    public static class SupplierServiceEntryMiniAppBuilder {
        private String params;
        private String path;
        private String appId;

        public SupplierServiceEntryMiniAppBuilder params(String params) {
            this.params = params;
            return this;
        }

        public SupplierServiceEntryMiniAppBuilder path(String path) {
            this.path = path;
            return this;
        }

        public SupplierServiceEntryMiniAppBuilder appId(String appId) {
            this.appId = appId;
            return this;
        }

        public EntryMiniApp build() {
            EntryMiniApp entryMiniApp = new EntryMiniApp();
            entryMiniApp.setParams(params);
            entryMiniApp.setPath(path);
            entryMiniApp.setApp_id(appId);
            return entryMiniApp;
        }
    }


    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

}
