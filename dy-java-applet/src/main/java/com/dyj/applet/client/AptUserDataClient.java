package com.dyj.applet.client;

import com.dtflys.forest.annotation.BaseRequest;
import com.dtflys.forest.annotation.Get;
import com.dtflys.forest.annotation.Query;
import com.dtflys.forest.annotation.Var;
import com.dyj.applet.domain.vo.*;
import com.dyj.common.domain.DyResult;
import com.dyj.common.domain.query.UserInfoQuery;
import com.dyj.common.interceptor.BizTokenHeaderInterceptor;
import com.dyj.common.interceptor.TokenHeaderInterceptor;

/**
 * 用户信息
 * 用户抖音主页数据
 *
 * @author danmo
 * @date 2024-05-24 13:51
 **/
@BaseRequest(baseURL = "${domain}")
public interface AptUserDataClient {

    /**
     * 获取用户视频情况
     *
     * @param query    用户信息
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId   通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserItemVo>
     */
    @Get(url = "/api/apps/v1/user/get_item/", interceptor = TokenHeaderInterceptor.class)
    DyResult<AptUserItemVo> getUserItem(@Var("query") UserInfoQuery query, @Query("date_type") Long dataType, @Query("open_id") String openId);


    /**
     * 获取用户视频情况(经营授权)
     *
     * @param query    用户信息
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId   通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserItemVo>
     */
    @Get(url = "/api/apps/v1/user_bc/get_item/", interceptor = BizTokenHeaderInterceptor.class)
    DyResult<AptUserItemVo> getUserBizItem(@Var("query") UserInfoQuery query, @Query("date_type") Long dataType, @Query("open_id") String openId);

    /**
     * 获取用户粉丝数
     *
     * @param query    用户信息
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId   通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserFansVo>
     */
    @Get(url = "/api/apps/v1/user/get_fans/", interceptor = TokenHeaderInterceptor.class)
    DyResult<AptUserFansVo> getUserFans(@Var("query") UserInfoQuery query, @Query("date_type") Long dataType, @Query("open_id") String openId);

    /**
     * 获取用户粉丝数(经营授权)
     *
     * @param query    用户信息
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId   通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserFansVo>
     */
    @Get(url = "/api/apps/v1/user_bc/get_fans/", interceptor = BizTokenHeaderInterceptor.class)
    DyResult<AptUserFansVo> getUserBizFans(@Var("query") UserInfoQuery query, @Query("date_type") Long dataType, @Query("open_id") String openId);

    /**
     * 获取用户点赞数
     *
     * @param query    用户信息
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId   通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserLikeVo>
     */
    @Get(url = "/api/apps/v1/user/get_like/", interceptor = TokenHeaderInterceptor.class)
    DyResult<AptUserLikeVo> getUserLike(@Var("query") UserInfoQuery query, @Query("date_type") Long dataType, @Query("open_id") String openId);

    /**
     * 获取用户点赞数(经营授权)
     *
     * @param query    用户信息
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId   通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserLikeVo>
     */
    @Get(url = "/api/apps/v1/user_bc/get_like/", interceptor = BizTokenHeaderInterceptor.class)
    DyResult<AptUserLikeVo> getUserBizLike(@Var("query") UserInfoQuery query, @Query("date_type") Long dataType, @Query("open_id") String openId);

    /**
     * 获取用户评论数
     * @param query 用户信息
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId 通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserCommentVo>
     */
    @Get(url = "/api/apps/v1/user/get_comment/", interceptor = TokenHeaderInterceptor.class)
    DyResult<AptUserCommentVo> getUserComment(@Var("query") UserInfoQuery query, @Query("date_type") Long dataType, @Query("open_id") String openId);

    /**
     * 获取用户评论数(经营授权)
     * @param query 用户信息
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId 通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserCommentVo>
     */
    @Get(url = "/api/apps/v1/user_bc/get_comment/", interceptor = BizTokenHeaderInterceptor.class)
    DyResult<AptUserCommentVo> getUserBizComment(@Var("query") UserInfoQuery query, @Query("date_type") Long dataType, @Query("open_id") String openId);

    /**
     * 获取用户分享数
     * @param query 用户信息
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId 通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserShareVo>
     */
    @Get(url = "/api/apps/v1/user/get_share/", interceptor = TokenHeaderInterceptor.class)
    DyResult<AptUserShareVo> getUserShare(@Var("query") UserInfoQuery query, @Query("date_type") Long dataType, @Query("open_id") String openId);

    /**
     * 获取用户分享数(经营授权)
     * @param query 用户信息
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId 通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserShareVo>
     */
    @Get(url = "/api/apps/v1/user_bc/get_share/", interceptor = BizTokenHeaderInterceptor.class)
    DyResult<AptUserShareVo> getUserBizShare(@Var("query") UserInfoQuery query, @Query("date_type") Long dataType, @Query("open_id") String openId);

    /**
     * 获取用户主页访问数
     * @param query 用户信息
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId 通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserProfileVo>
     */
    @Get(url = "/api/apps/v1/user/get_profile/", interceptor = TokenHeaderInterceptor.class)
    DyResult<AptUserProfileVo> getUserProfile(@Var("query") UserInfoQuery query, @Query("date_type") Long dataType, @Query("open_id") String openId);

    /**
     * 获取用户主页访问数(经营授权)
     * @param query 用户信息
     * @param dataType 近7/15天；输入7代表7天、15代表15天
     * @param openId 通过code2Session获取，用户唯一标志
     * @return DyResult<AptUserProfileVo>
     */
    @Get(url = "/api/apps/v1/user_bc/get_profile/", interceptor = BizTokenHeaderInterceptor.class)
    DyResult<AptUserProfileVo> getUserBizProfile(@Var("query") UserInfoQuery query, @Query("date_type") Long dataType, @Query("open_id") String openId);

}
