package com.dyj.applet.client;

import com.dtflys.forest.annotation.BaseRequest;
import com.dtflys.forest.annotation.JSONBody;
import com.dtflys.forest.annotation.Post;
import com.dyj.applet.domain.query.LimitOpPointQuery;
import com.dyj.applet.domain.query.OrderCizStatusQuery;
import com.dyj.applet.domain.query.RegisterMaAppQuery;
import com.dyj.applet.domain.query.ShopMemberLeaveQuery;
import com.dyj.applet.domain.vo.OrderCizStatusVo;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.vo.BaseVo;
import com.dyj.common.interceptor.ClientTokenInterceptor;

/**
 * 电商小程序
 *
 * @author danmo
 * @date 2024-04-28 10:17
 **/
@BaseRequest(baseURL = "${domain}")
public interface AptEAppletClient {

    /**
     * 注册小程序积分阈值
     *
     * @param query 入参
     * @return DySimpleResult
     */
    @Post(url = "${limitOpPoint}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<BaseVo> limitOpPoint(@JSONBody LimitOpPointQuery query);

    /**
     * 注册小程序预览图、定制类小程序开发者注册信息
     *
     * @param query 入参
     * @return DySimpleResult
     */
    @Post(url = "${registerMaApp}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<BaseVo> registerMaApp(@JSONBody RegisterMaAppQuery query);

    /**
     * 查询订单的定制完成状态
     *
     * @param query 入参
     * @return DySimpleResult<OrderCizStatusVo>
     */
    @Post(url = "${queryOrderCustomizationStatus}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<OrderCizStatusVo> queryOrderCustomizationStatus(@JSONBody OrderCizStatusQuery query);

    /**
     * 退会
     * @param query
     * @return DySimpleResult<BaseVo>
     */
    @Post(url = "${shopMemberLeave}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<BaseVo> shopMemberLeave(@JSONBody ShopMemberLeaveQuery query);
}
