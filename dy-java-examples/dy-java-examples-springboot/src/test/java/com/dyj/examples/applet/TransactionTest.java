package com.dyj.examples.applet;

import com.alibaba.fastjson.JSON;
import com.dyj.applet.DyAppletClient;
import com.dyj.applet.domain.GetFundBillQuery;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.QueryWithdrawOrderQuery;
import com.dyj.examples.DyJavaExamplesApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 交易系统测试
 */
@EnableAutoConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DyJavaExamplesApplication.class)
public class TransactionTest {


    /**
     * 进件图片上传
     */
    @Test
    public void merchantImageUpload(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.merchantImageUpload("jpg",null)
                )
        );
    }

    /**
     * 发起进件
     */
    @Test
    public void createSubMerchantV3(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.createSubMerchantV3(CreateSubMerchantMerchantQuery.builder().build())
                )
        );
    }

    /**
     * 进件查询
     */
    @Test
    public void queryMerchantStatusV3(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryMerchantStatusV3(QueryMerchantStatusMerchantQuery.builder()
                                .merchantId("1")
                                .appId("1")
                                .build())
                )
        );
    }

    /**
     * 开发者获取小程序收款商户/合作方进件页面
     */
    @Test
    public void openAppAddSubMerchant(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.openAppAddSubMerchant(GetMerchantUrlMerchantQuery.builder().build())
                )
        );
    }

    /**
     * 服务商获取小程序收款商户进件页面
     */
    @Test
    public void openAddAppMerchant(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.openAddAppMerchant(GetMerchantUrlMerchantQuery.builder().build())
                )
        );
    }

    /**
     * 服务商获取服务商进件页面
     */
    @Test
    public void openSaasAddMerchant(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.openSaasAddMerchant(OpenSaasAddMerchantMerchantQuery.builder().build())
                )
        );
    }

    /**
     * 服务商获取合作方进件页面
     */
    @Test
    public void openSaasAddSubMerchant(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.openSaasAddSubMerchant(OpenSaasAddSubMerchantMerchantQuery.builder().build())
                )
        );
    }

    /**
     * 查询标签组信息
     */
    @Test
    public void tagQuery(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.tagQuery(TagQueryQuery.builder().build())
                )
        );
    }

    /**
     * 查询CPS信息
     */
    @Test
    public void queryCps(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryCps(QueryTransactionCpsQuery.builder().build())
                )
        );
    }

    /**
     * 查询订单信息
     */
    @Test
    public void queryOrder(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryOrder(QueryTransactionOrderQuery.builder().build())
                )
        );
    }

    /**
     * 发起退款
     */
    @Test
    public void createRefund(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.createRefund(CreateRefundQuery.builder().build())
                )
        );
    }

    /**
     * 查询退款
     */
    @Test
    public void queryRefund(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryRefund(QueryRefundQuery.builder().build())
                )
        );
    }

    /**
     * 同步退款审核结果
     */
    @Test
    public void merchantAuditCallback(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.merchantAuditCallback(MerchantAuditCallbackQuery.builder().build())
                )
        );
    }

    /**
     * 推送履约状态
     */
    @Test
    public void fulfillPushStatus(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.fulfillPushStatus(FulfillPushStatusQuery.builder().build())
                )
        );
    }

    /**
     * 发起分账
     */
    @Test
    public void createSettle(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.createSettle(CreateSettleQuery.builder().build())
                )
        );
    }

    /**
     * 查询分账
     */
    @Test
    public void querySettle(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.querySettle(QuerySettleQuery.builder().build())
                )
        );
    }

    /**
     * 商户余额查询
     */
    @Test
    public void queryChannelBalanceAccount(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryChannelBalanceAccount(QueryChannelBalanceAccountQuery.builder().build())
                )
        );
    }

    /**
     * 商户提现
     */
    @Test
    public void merchantWithdraw(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.merchantWithdraw(MerchantWithdrawQuery.builder().build())
                )
        );
    }

    /**
     * 商户提现结果查询
     */
    @Test
    public void queryWithdrawOrder(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryWithdrawOrder(QueryWithdrawOrderQuery.builder().build())
                )
        );
    }

    /**
     * 开发者获取小程序收款商户/合作方提现页面
     */
    @Test
    public void saasAppAddSubMerchant(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.saasAppAddSubMerchant(SaasAppAddSubMerchantQuery.builder().build())
                )
        );
    }

    /**
     * 服务商获取小程序收款商户提现页面
     */
    @Test
    public void saasGetAppMerchant(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.saasGetAppMerchant(OpenAddAppMerchantMerchantQuery.builder().build())
                )
        );
    }

    /**
     * 服务商获取服务商提现页面
     */
    @Test
    public void saasAddMerchant(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.saasAddMerchant(SaasAddMerchantQuery.builder().build())
                )
        );
    }

    /**
     * 服务商获取合作方提现页面
     */
    @Test
    public void saasAddSubMerchant(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.saasAddSubMerchant(OpenSaasAddSubMerchantMerchantQuery.builder().build())
                )
        );
    }

    /**
     * 获取资金账单
     */
    @Test
    public void getFundBill(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.getFundBill(GetFundBillQuery.builder().build())
                )
        );
    }

    /**
     * 获取交易账单
     */
    @Test
    public void getBill(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.getBill(GetBillQuery.builder().build())
                )
        );
    }

}
